#!/bin/bash
## take entities as input, download images & voices, and generate csv
## ./download-icon-voice.sh entities.csv output.csv
INPUT=$1
SAMPLERATE=$2
OUTPUT=$3
if [ -f "$OUTPUT" ]; then
  rm "$OUTPUT"
fi
cnt=0
while read contact
do
  let cnt=cnt+1
  if [ $cnt -eq $SAMPLERATE ]
  then
	  echo "$contact" >> "$OUTPUT"
          let cnt=0
  fi
done <"$INPUT"
