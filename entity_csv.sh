#!/bin/bash
## create a csv file for mysql load from the folder contains all the PNG icons
## Example usage:
FOLDER=$1
OUTPUT=$2
for f in $FOLDER/*.png
do
        echo "Processing $f"
        lower=`echo $f | tr '[:upper:]' '[:lower:]'`
        tmp_name=`echo "$lower" |  sed -r 's/logo_//g' | sed -r 's/disney_//g'`
        echo $tmp_name
        mv $f $tmp_name
        new_name=`echo "$tmp_name" |  sed -r 's/[_\.]+/ /g'`
        echo $new_name
         tokens=(  $new_name )
          unset tokens[${#tokens[@]}-1]
        len=${#tokens[@]}
        cnt=0
        fname=""
        echo $len
        while [ $cnt -lt ${len} ]; do
           if [ $cnt -gt 0 ]; then
             fname+=" "
           fi
           echo ${tokens[$cnt]}
           fname+=${tokens[$cnt]}
           let cnt=cnt+1
        done
        echo "$fname,$lower" >> $OUTPUT 
done
