<?php
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

// get the q parameter from URL
$q = trim(strtolower($_REQUEST["q"]));
$context = $_REQUEST["c"];
$general = $_REQUEST["g"];
$lang = $_REQUEST["lang"];

// Connecting, selecting database
$link = mysql_connect('localhost', 'root', 'PenCanFly') or die('Could not connect: ' . mysql_error());

mysql_set_charset('utf8', $link);
mysql_select_db('wordpress-db') or die('Could not select database');

$response = array();
$context_entities = array();
$general_entities = array();
$root_category = array("art",
                               "technology",
                               "health",
                               "science",
                               "music",
                               "math",
                               "reading and writing",
                               "cartoons",
                               "english",
                               "game",
                               "social studies",
                               "crafting",
                               "preschool",
                               "root category",
                               );

if ($context == "1") {
	if (strpos($q, ".png")) {
			$select = "SELECT t1.related_entity as filename, t2.name_" . $lang . " as name, t1.relatedness as score  FROM  entity_graph as t1 JOIN entity_icons as t2  where t2.name_" . $lang . " IS NOT NULL and t1.related_entity = t2.filename  AND  t1.entity_filename = '" . $q . "'   ORDER BY score desc";
				error_log("select:" . $select); 

			$result = mysql_query($select, $link);

			if (!$result) { 
				error_log("query messed up with:" . $select); 
			} else {
				$count=mysql_num_rows($result);

				if($count>0) {
				  while($row = mysql_fetch_assoc($result)) {
				     array_push($context_entities, $row["filename"] . "(" . $row["name"] . ")");
				  }
				}
			}
        }
     	if (count($context_entities) > 0) { 
                $response["context"] = implode(",", $context_entities);
   	}
}
if ($general == "1") {
	    for ($x = 0; $x < count($root_category); $x++) {
		$category = $root_category[$x];
                $select = "SELECT filename, name_" . $lang . " as name,  clkcnt/10 + priorscore as score FROM entity_icons WHERE name_" . $lang . " IS NOT NULL and root_cat = '" . $root_category[$x] . "'  ORDER BY score desc";


		$result = mysql_query($select, $link);

                if (!$result) { 
			error_log("query messed up with:" . $select); 
		} else {
			$count = mysql_num_rows($result);

			$entity_list = array();
			if($count>0) {
			  while($row = mysql_fetch_assoc($result)) {
				array_push($entity_list, $row["filename"] . "(" . $row["name"] . ")");
			  }
			}
		}
	 	array_push($general_entities, '[' .implode("," , $entity_list) . ']');
	   } 
   	   $response["general"] = implode(",", $general_entities);
}


mysql_close($link);
				error_log("response:" . json_encode($response));

print json_encode($response);

?>
