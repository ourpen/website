<?php
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

// get the q parameter from URL
$q = trim(strtolower($_REQUEST["q"]));
$getip = $_REQUEST["getip"];
$context = $_REQUEST["c"];
$general = $_REQUEST["g"];
$history = $_REQUEST["h"];
$lang = $_REQUEST["lang"];

// Connecting, selecting database
$link = mysql_connect('localhost', 'root', 'PenCanFly') or die('Could not connect: ' . mysql_error());

mysql_set_charset('utf8', $link);
mysql_select_db('wordpress-db-test') or die('Could not select database');

$response = array();
$context_entities = array();
$general_entities = array();
$root_category_icons = array("art.png",
                               "technology.png",
                               "health.png",
                               "science.png",
                               "music.png",
                               "math.png",
                               "reading_and_writing.png",
                               "cartoons.png",
                               "english.png",
                               "game.png",
                               "social_studies.png",
                               "crafting.png",
                               "preschool.png",
                               );
$root_category = array("art",
                               "technology",
                               "health",
                               "science",
                               "music",
                               "math",
                               "reading and writing",
                               "cartoons",
                               "english",
                               "game",
                               "social studies",
                               "crafting",
                               "preschool",
                               );

$ip = "";
if ($getip == "developer") {
  $ip = "developer";
  $response["ip"] = $ip;
} else if ($getip == "1") {
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	  $ip=$_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
	  $ip=$_SERVER['REMOTE_ADDR'];
	}
        $response["ip"] = $ip;
} else if ($getip != "0") {
  $ip = $getip;
}

if ($context == "1") {
	if (strpos($q, ".png")) {
			$select = "SELECT t1.related_entity as filename, t2.name_" . $lang . " as name, t1.relatedness as score  FROM  entity_graph as t1 JOIN entity_icons as t2  where t2.name_" . $lang . " IS NOT NULL and t1.related_entity = t2.filename  AND  t1.entity_filename = '" . $q . "'   ORDER BY score desc";

			$result = mysql_query($select, $link);

			if (!$result) { 
				error_log("query messed up with:" . $select); 
			} else {
				$count=mysql_num_rows($result);

				if($count>0) {
				  while($row = mysql_fetch_assoc($result)) {
				     array_push($context_entities, $row["filename"] . "(" . $row["name"] . ")");
				  }
				}
			}
	} else {
           if ($q == "") {
		   $root_category_set = '"' . implode('", "', $root_category_icons) . '"';
		   $select = "SELECT  filename, name_" . $lang . " as name FROM  entity_icons where name_" . $lang . " IS NOT NULL and filename IN (" . $root_category_set . ")";
		   $result = mysql_query($select, $link);
		   if (!$result) { 
			error_log("query messed up with:" . $select); 
		   } else {
			   $count=mysql_num_rows($result);

			   if($count>0) {
			      while($row = mysql_fetch_assoc($result)) {
				array_push($context_entities, $row["filename"] . "(" . $row["name"] . ")");
			      }
			   }
		   }
           } else {
			$select = 'SELECT filename, name_' . $lang . ' as name from entity_icons WHERE name_' . $lang . ' like "' . $q . '%"';
                        if (strlen($q) >= 3) {
                          $select .= (' OR name_' . $lang . ' like "%' . $q . '%"');  
                        }

			$result = mysql_query($select, $link);

		        if (!$result) { 
				error_log("query messed up with:" . $select); 
			} else {
				$count=mysql_num_rows($result);

				if($count>0) {
				  while($row = mysql_fetch_assoc($result)) {
				     array_push($context_entities, $row["filename"] . "(" . $row["name"] . ")");
				  }
				}
			}
           }
        }
     	if (count($context_entities) > 0) { 
                $response["context"] = implode(",", $context_entities);
   	}
}

if ($general == "1") {
	// lookup all hints from array if $q is different from "" 
        if ($ip !== "") {
                $user_vocab = array();
                $user_per_cat_vocab = array();
		$select = "SELECT t2.filename as filename, t2.name_" . $lang . " as name, t2.root_cat as category, t1.querycnt + t2.clkcnt/10 + t2.priorscore/10 as score FROM user_vocab as t1 JOIN entity_icons as t2 WHERE t2.name_" . $lang . " IS NOT NULL and t1.id = '" . $ip . "' and t1.word = t2.name_" . $lang . " ORDER BY score desc limit 10";

		$result = mysql_query($select, $link);

		if (!$result) { 
			error_log("query messed up with:" . $select); 
		} else {
			$count=mysql_num_rows($result);
			
			if($count>0) {
			  while($row = mysql_fetch_assoc($result)) {
			     $user_vocab[$row["filename"]] = true;
			     if(isset($user_per_cat_vocab[$row["category"]])) {
				$user_per_cat_vocab[$row["category"]] = $user_per_cat_vocab[$row["category"]] . "," . $row["filename"] . "(" . $row["name"] . ")";
			     } else {
				$user_per_cat_vocab[$row["category"]] =  $row["filename"] . "(" . $row["name"] . ")";
			     }
			  }
			}
		}
		$user_profile = array();
		$user_profile_map = array();
                 
		$select = "SELECT category, querycnt as score FROM user_profile  WHERE id = '" . $ip . "' and category != 'root category' ORDER BY score desc limit 20";
		$result = mysql_query($select, $link);

		if (!$result) { 
			error_log("query messed up with:" . $select); 
		} else {
			$count=mysql_num_rows($result);

			if($count>0) {
			  while($row = mysql_fetch_assoc($result)) {
			     if ($row["category"] != "") {
				     array_push($user_profile, $row["category"]);
				     $user_profile_map[$row["category"]] = true;
			     }
			  }
			}
		}

                for ($x = 0; $x < count($user_profile); $x++) {
			$select = "SELECT filename, name_" . $lang . " as name,  clkcnt/10 + priorscore as score FROM  entity_icons WHERE name_" . $lang . " IS NOT NULL and root_cat = '" . $user_profile[$x] . "'  ORDER BY score desc limit 20";
			$result = mysql_query($select, $link);

			if (!$result) { 
				error_log("query messed up with:" . $select); 
			} else {
				$count = mysql_num_rows($result);

				$entity_list = array();
				if($count>0) {
				  while($row = mysql_fetch_assoc($result)) {
					$entity_icon = $row["filename"];
					if(!isset($user_vocab[$entity_icon])) {
					  array_push($entity_list, $row["filename"] . "(" . $row["name"] . ")");
					}
				  }
				}
			}
		        $per_cat_entities = "";
                        if (isset($user_per_cat_vocab[$user_profile[$x]])) {
                          $per_cat_entities .= ($user_per_cat_vocab[$user_profile[$x]] . ",");
                        }
                        $per_cat_entities .= implode("," , $entity_list);
                        array_push($general_entities, '[' . $per_cat_entities . ']');
		}
            } 

	    for ($x = 0; $x < count($root_category); $x++) {
		$category = $root_category[$x];
		if (isset($user_profile_map[$category])) {
		  continue;
		} 
		$select = "SELECT filename, name_" . $lang . " as name,  clkcnt/10 + priorscore as score FROM entity_icons WHERE name_" . $lang . " IS NOT NULL and root_cat = '" . $root_category[$x] . "'  ORDER BY score desc limit 20";
		$result = mysql_query($select, $link);

                if (!$result) { 
			error_log("query messed up with:" . $select); 
		} else {
			$count = mysql_num_rows($result);

			$entity_list = array();
			if($count>0) {
			  while($row = mysql_fetch_assoc($result)) {
				array_push($entity_list, $row["filename"] . "(" . $row["name"] . ")");
			  }
			}
		}
	 	array_push($general_entities, '[' .implode("," , $entity_list) . ']');
	   } 
   	   $response["general"] = implode(",", $general_entities);
}

if ($history == "1") {
        if ($ip !== "") {
                $watch_history = array();
                $entity_history = array();
                $select = 'select distinct click_url as url, title, displaylink from open_logging where ip = "' . $ip . '" and click_url IS NOT NULL and title IS NOT NULL and displaylink IS NOT NULL ORDER BY time desc limit 16';
		$result = mysql_query($select, $link);

		if (!$result) { 
			error_log("query messed up with:" . $select); 
		} else {
			$count=mysql_num_rows($result);
			
			if($count>0) {
			  while($row = mysql_fetch_assoc($result)) {
                                $url = $row["url"]; 
                                $ytid = $url;
                                $pos = strrpos($url, "=");
                                if ($pos) {
                                  $ytid = substr($url, $pos + 1);
                                }
                                array_push($watch_history, $ytid . ";" . $row["title"] . ";" . $row["displaylink"]); 
			  }
			}
		}

                
                $select = 'select  t2.filename as filename, t1.query as name, sum( 1 - (CURDATE() - DATE(t1.time)) / (CURDATE() - DATE(t1.time) + 7)) as score from open_logging t1 JOIN entity_icons t2 ON t1.query = t2.name_' . $lang . ' where   IP = "'. $ip . '" GROUP BY filename ORDER BY score desc limit 20';
		$result = mysql_query($select, $link);

		if (!$result) { 
			error_log("query messed up with:" . $select); 
		} else {
			$count=mysql_num_rows($result);
			
			if($count>0) {
			  while($row = mysql_fetch_assoc($result)) {
                                $filename = $row["filename"]; 
                                array_push($entity_history, $filename . "(" . $row["name"] . ")");
			  }
			}
		}
        }

   	$response["watch_history"] = implode(",", $watch_history);
   	$response["entity_history"] = implode(",", $entity_history);
}

mysql_close($link);

print json_encode($response);

?>
