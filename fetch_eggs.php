<?php
$q = $_REQUEST["q"];
$digests = $_REQUEST["digests"];
$digests = trim(strtolower($digests));
/*
$channels = $_REQUEST["channels"];
$channels = trim($channels);
*/
$lang = $_REQUEST["lang"];

// Connecting, selecting database
$link = mysql_connect('localhost', 'root', 'PenCanFly')
    or die('Could not connect: ' . mysql_error());
mysql_set_charset('utf8', $link);
mysql_select_db('wordpress-db') or die('Could not select database');

$digest_arr = preg_split("/,/", $digests);
$entity_hashset = array();
foreach ($digest_arr as $digest) {
   $tokens = preg_split("/[\s\.|]+/", trim($digest));
   $first_key = True;
   $prev_token = '';
   $unigrams = array();
   $bigrams = array();
   $trigrams = array();
   foreach ($tokens as $orig_token) {
	$token = trim($orig_token);
        if (!$token || $token == "") { continue; }
        if (!$first_key) {
          array_push($bigrams, $prev_token . " " . $token);
        } else {
          $first_key = False;
        }
        array_push($unigrams, $token);
        $prev_token = $token;
   }
       
   // TODO: need support CJK here, I assume it is now broken. 
   $phrase_set  = implode("\", \"", array_merge($unigrams, $bigrams)); 
   $select = 'SELECT filename, name_' . $lang . ' as name FROM entity_icons WHERE  name_' . $lang . ' IN ("'. $phrase_set . '")';
		// error_log("select:" . $select); 

	$result = mysql_query($select, $link);
	if (!$result) { 
		error_log("query messed up with:" . $select); 
	} else {
		$count=mysql_num_rows($result);
		if($count>0) {
		  while($row = mysql_fetch_assoc($result)) {
		     $entity_string = $row["filename"] . "(" . $row["name"] . ")";
		     if (strpos($entity_string, ",") !== false) {
			// entity_string should not contains comma, it breaks the protocol.
			continue;
		     }
		     if (!array_key_exists($entity_string, $entity_hashset)){
			$entity_hashset[$entity_string] = 1;
		     } else {
			$entity_hashset[$entity_string] = $entity_hashset[$entity_string] + 1;
		     }
		  }	
		}
        }
}
arsort($entity_hashset);
$hint = implode(",", array_keys($entity_hashset));

echo $hint;

mysql_close($link);

?>
