var myip = "";
var deb_level;
var url_weight = 5;
var title_weight = 4;
var snippet_weight = 2;
var smooth = 0;
var readdb = 1;
var devphp_set = {};
var context; 
var current_page = 0;
var cgi_q = "";
var documentClick = false;
var watch_history = [];
var entity_history = [];
var referrer =  document.referrer;
var baseURI =  document.baseURI;
var documentURI =  document.documentURI;
var digest = {};
var isIOS = ((/iphone|ipad/gi).test(navigator.appVersion));
var ui_status = 1; // 0: entity selection; 1: search result page; 2: challenge page 3:watchpage
var deft_query = "DIY OR lego set OR mad science OR nursery rhymes OR cartoon OR ESL OR spelling bee OR making cake OR barbie OR math OR health";
var srp_results_per_row = 1;
var entity_term_map = {};
var total_entities = 0;
var total_cse_responses = 0;
var entity_result_map = {};

function positionWeight(pos, smooth) {
   return 1.0 / (pos+smooth);
}

function digestGoogleSearchResponse(response) {
        var out = "";
        digest = {};
        if (!response.items) {
          return;
        }
	var query = response.queries.request[0].searchTerms; 

        var term_map = {};
	if (entity_term_map.hasOwnProperty(query)) {
		   term_map = entity_term_map[query];
	}
        for(var i = 0; i < response.items.length; i++) {
            var position = i + 1; 
            var item = response.items[i];
            var result = parseGoogleSearchResponse(item, term_map, positionWeight(position, smooth));
            if (!result.hasOwnProperty("url") | !result.hasOwnProperty("title") || !result.hasOwnProperty("snippet")) {
              continue;
            }
	    var logging_params = {
	      "table" : "cse_scraping",
	      "query" : query,
	      "position" :  position,
	      "url" :  result["url"],
	      "title" :  result["title"],
	      "snippet" :  result["snippet"],
	    }
	    var php_file = ToggleDevPhp("cse_logging");
	    $.post(php_file, logging_params);
        }
        var vec = composeWeightSortedTermVec(term_map);
	var new_map = composeTermMapFromTermVec(vec, 100);
	entity_term_map[query] = new_map;

        for (term in new_map) {
	    var logging_params = {
	      "table" : "entity_termvec",
	      "query" : query,
	      "term" : term,
	      "weight" : new_map[term].toString(),
	    }
	    var php_file = ToggleDevPhp("cse_logging");
	    $.post(php_file, logging_params);
        }
        total_cse_responses++;
    
}

function readFeatureIntoTermMap(features, feature_weight, pos_weight, term_map) {
    for (var x = 0; x < features.length; x++) {
      var term = features[x].toLowerCase().trim();
      if (term == "" || term.length <= 1 || stopwords_dict.hasOwnProperty(term)) {
        continue;
      }
      if (term_map.hasOwnProperty(term)) {
         term_map[term] += feature_weight * pos_weight;
      } else {
         term_map[term] = feature_weight * pos_weight;
      }
    }
}

function sanitizeText(text) {    
  return text.toLowerCase().trim().replace(/[\?.,\/#!$%\^&\*;:{}=\-_`~()|\"\'`“]/g, " ");
}

function parseGoogleSearchResponse(item, term_map, pos_weight) {
    var result = {};
    if (deb_level && deb_level > 1) {
	    result["debug"] = JSON.stringify(item);
    }
    if (!item.title || !item.link || !item.snippet) {
      return result;
    }
    var title_clean_txt = sanitizeText(item.title);
    var snippet_clean_txt = sanitizeText(item.snippet);
    var url_clean_txt = sanitizeText(item.link);
    var title_terms = title_clean_txt.split(" ");
    var snippet_terms = snippet_clean_txt.split(" ");
    var url_terms = url_clean_txt.split(" ");
    result["title"] = title_clean_txt;
    result["url"] = url_clean_txt;
    result["snippet"] = snippet_clean_txt;
    readFeatureIntoTermMap(url_terms, url_weight, pos_weight, term_map);
    readFeatureIntoTermMap(title_terms, title_weight, pos_weight, term_map);
    readFeatureIntoTermMap(snippet_terms, snippet_weight, pos_weight, term_map);
    return result;
}

function handleGoogleSearchResponse(response, status) {
	digestGoogleSearchResponse(response);
}

function GoogleCSESearch(query, lowrange) {
      var lang = $(".lang_selection").val();
      var url = "https://www.googleapis.com/customsearch/v1?";
      url += "&cx=" + "014484386212996657006:bzam1dkgrs4";
      url += "&key=" + "AIzaSyBt_9oYNnRByBH-6jd4O3a2-vkG-OHsJYk";

      query = query.replace(/\s+/g, "%20");
      url += "&q=" + query;
      if (lowrange >= 10) {
	      url += "&lowRange=" + lowrange.toString();
      }
      $.get(url, handleGoogleSearchResponse);
}


// IN: term_map contains all terms for a key(query)
// OUT: a vector with terms sorted by weight
function composeWeightSortedTermVec(term_map) {
  var vec = [];
  for (var key in term_map) {
     vec.push({term: key, weight:term_map[key]});
  }
  vec.sort(function(a, b) {
   return b.weight - a.weight;
  });
  return vec;
}

// IN: a vector with terms sorted by weight
// OUT: term_map contains only most important terms up to cutoff length
function composeTermMapFromTermVec(term_vec, cutoff) {
  var term_map = {};
  var length = Math.min(term_vec.length, cutoff);
  var vec_scale = 0;
  for (var i = 0; i < length; i++) {
     var kv = term_vec[i];
     term_map[kv.term] = kv.weight;
     vec_scale += kv.weight * kv.weight;
  }
  term_map["[scale]"] = Math.sqrt(vec_scale);
  return term_map;
}

// map_a and map_b are equal length
// each suppose to contains the most important terms for a key (query).
// OUT: cosine distance
function cosineSimilarity(map_a, map_b) {
  var dot_product = 0;

  for (var term in map_a) {
    if (term == "[scale]") {
      continue;
    }
    if (map_b.hasOwnProperty(term)) {
       dot_product += map_a[term] * map_b[term];
    }
  }
  return dot_product / (map_a["[scale]"] * map_b["[scale]"]);  
}

// According to some stackflow page, this can prevent unintentional touchmove become a click event
$(document).on('touchstart', function() {
    documentClick = true;
});
$(document).on('touchmove', function() {
    documentClick = false;
});

// A util to retrieve and set a list off css property
(function($)
{
    $.fn.cssAll = function(css)
    {
        var obj = {};

        if(this.length)
        {
            var css = css.split(',');
            var params = [];

            for(var i=0,ii=css.length; i<ii; i++)
            {
                params = css[i].split(':');

                obj[$.trim(params[0])] = $(this).css($.trim(params[1] || params[0]));
            }
        }

        return obj;
    };
})(jQuery);

function embedYTLink(ytid) {
  return "https://www.youtube.com/embed/" + ytid + "?&autoplay=1&rel=0&fs=0&showinfo=1&autohide=1&modestbranding=1&enablejsapi=1";
}

function watchYTLink(ytid) {
  return "https://www.youtube.com/watch?v=" + ytid;
}

function thumbnailYTLink(ytid) {
  return "https://i.ytimg.com/vi/" + ytid + "/mqdefault.jpg"
}


function queryEntityImgHtml(img_file) {
    return '<img class="queryEntityImg" src="' + img_file + '">';
}	     

function queryEntityTextHtml(txt) {
    return '<b>' + txt + '</b>';
}	     
 
 
function composeWatchpageTopBar() {
  var html = ""
  html += '<div class="watchpage_navigation">';
  html += '<button class="back_to_kg">';
  html += '<img class="vertical_img" src="icons/open/knowledge_entity.jpg"/>';
  html += '</button>';
  html += '<button class="back_to_hp">';
  html += '<img class="vertical_img" src="icons/open/red_magnifier.png"/>';
  html += '</button>';
  html += '</div> <!-- watchpage_navigation -->';
  html += '<div class="andquery_entities">';
  for (var x = 0; x < andQuery_entities.length; x++) {    
    var entity = andQuery_entities[x];
    var icon = iconFromUserQueryEntity(entity);
    if (icon != "") {
	    html += queryEntityImgHtml("entity_icons/" + icon);
    } else {
	    var entity_name = entityNameFromUserQueryEntity(andQuery_entities[x]);
            if (entity_name != "video") {
		    html += queryEntityTextHtml(entityNameFromUserQueryEntity(andQuery_entities[x]));
            }
    }      
  }
  html += '</div><!-- andquery_entities -->';
  return html;
}

function composeRefinementBar() {
   var html = '<div class="refinementbar">';
   html += '<div class="related_search"> <input type="text" class="refinement_query" name="refine_q" id="refine_q" size="10"  value=""/></div>';
   html += '<div class="refinements">'; 
   var cnt = 0;
   var entity_width = parseInt($(".entity_icon_button").css("width").slice(0, -2));
   var entities_per_row = 0.8 * Math.floor($(document).width()) / entity_width;
   for(var i=0; i < result_entities.length; i++) {
     // entity_string format "<icon_file>(<entity_name>)"
     var entity = result_entities[i];
     var icon = iconFromUserQueryEntity(entity);
     var name = entityNameFromUserQueryEntity(entity);
     var cur_query = iconFromUserQueryEntity(andQuery_entities[0]);
     if (icon == "" || name == "" || icon == cur_query) {
       continue;
     }
     html += entityHtmlFromToken(icon, name);
     if (++cnt >= entities_per_row) {
       break;
     }
   }
   for(var i=0; i < Math.min(entities_per_row - cnt, context_entities.length); i++) {
     var entity_icon = context_entities[i];
     html += entityHtmlFromToken(entity_icon, entity_icon2name[entity_icon]);
   }
   html += '</div> <!--refinements-->';
   html += '</div> <!--refinementbar-->';
   return html;
}

function composeIframeWindow(ytid) {	      
  return '<iframe class="iframe-window"  src="' + embedYTLink(ytid) + '" ></iframe>';
}

// Input: result with title, displaylink, ytid, thumbnail
function resultHtml(result, resulttype) {
     if (!result.hasOwnProperty("title") || result["title"] == "" ||
         !result.hasOwnProperty("displaylink") || result["displaylink"] == "" ||
         !result.hasOwnProperty("ytid") || result["ytid"] == "" ||
         !result.hasOwnProperty("thumbnail") || result["thumbnail"] == "") {
        return "";
     }
     var title_no_quote = result["title"].replace(/[;,\"\']/g, " ");
     var dlink_no_quote = result["displaylink"].replace(/[;,\"\']/g, " ");
     var html = '<div class="resultwrap" rtype="' + resulttype + '"  title="' +  title_no_quote + '" displaylink = "' + dlink_no_quote + '"   ytid="' + result["ytid"] + '">';
     html += '<div class="resulttitle"> <p class="titletxt">' + result["title"] + ' </p> </div>';
     html += '<div class="displaylink">' + result["displaylink"] + '</div>';
     html += '<div class="imagewrap">';
     html += "<div class=\"playbutton\"></div>";
     html += '<img class="result_thumbnail" src="' + result["thumbnail"] + '">';
     html += "</div> <!--imagewrap -->";
     html += "</div> <!--resulttype-->";

     return html;
}

function handleVideoClick(element) {
      var ytid = element.attr('ytid');
      if (ytid === "") {
	return;
      }
      var rtype = element.attr("rtype");
      if (curUI() == "wp") {
	$(".iframe").html(composeIframeWindow(ytid));
      } else {
              contextSwitchTo(3);
	      var watchpage_topbar = '<div class="watchpage_topbar">' + composeWatchpageTopBar() + '</div>';
	      var iframe = '<div class="iframe">' + composeIframeWindow(ytid) + '</div>';
	      var refine_entity_bar = composeRefinementBar();
	      var related_bar = '<div class="relatedbar">' + digest["content"] + '</div>';
	      $("#watchpage").html(watchpage_topbar + refine_entity_bar + iframe + related_bar);
      }

      var logging_params = fillInLoggingParams();
      logging_params["etype"] = (rtype == "srpresult" ? 'sc' : 'rc');
      logging_params["click_url"] = element.attr("ytid");  
      logging_params["title"] = element.attr("title");
      logging_params["displaylink"] = element.attr("displaylink");
      var myidx = element.index(".resultwrap");
      logging_params["click_pos"] = myidx;  
      var php_file = ToggleDevPhp("logging");
      $.post(php_file, logging_params);
}
 
// Parse YouTube search data API, response structure is different than google CSE API
function parseYoutubeSearchResponse(item) {
  var result = {};
  var title = item.snippet.title;
  if (title.length > 43) {
	  title = title.slice(0, 40) + "...";
  }
  result["title"] = title;
  result["thumbnail"] = item.snippet.thumbnails.medium.url;
  result["description"] =  item.snippet.description;
  result["ytid"] = item.id.videoId;
  var channelTitle = item.snippet.channelTitle;
  if (channelTitle.length > 24) {
    channelTitle = channelTitle.slice(0, 21) + "...";
  }
  result["displaylink"] = channelTitle;
  if (deb_level && deb_level > 1) {
    result["debug"] = JSON.stringify(item);
  }
  if (!result.hasOwnProperty("ytid") || !result.hasOwnProperty("title") || !result.hasOwnProperty("thumbnail") || !result.hasOwnProperty("displaylink")) {
    result["failed"] = true;
    if (deb_level && deb_level > 1) {
	result["debug"] += '<br/><b>Something is missing!</b><br/>';
    }
  }
  return result;
}

// See google CSE config here: https://cse.google.com/cse/setup/basic?cx=014484386212996657006%3A-hqy9dxsm08
var cse_cx = "014484386212996657006:-hqy9dxsm08";  // video
// Initials for each vertical, "k": knowledge entities. v:video, w:watch page, c:challenge
var vertical_name = ["kg", "vid",  "cha", "wp"];
var video_vertical = 1;
// Common prefix in case we want to debug a new CSE with different suffix
var cse_prefix = "014484386212996657006:";

var curse_words  = ["bull shit", "ass hole", "fuck", "fucks", "fucked", "fucking", "fucker", "anus",  "bitch", "asshole", "sucker", "damn", "fuckass", "motherfucker", "pussy", "shit", "idiot", "nigro", "niggers", "butt", "jackass", "testicle", "whore"];
var sex_seeking = ["xxx", "porn", "nake", "nak girl", "nud", "nak", "naked", "nude", "nudity", "blow job", "sex", "dick", "butt", "genital", "make love", "intercourse", "cock", "handjob", "humping", "penis", "vagina"];
var lgbt_words = ["gay", "transgender", "transgender", "homosexual", "lesbian", "queer", "asexual"];
var horror_phrases = ["ghost", "torture", "devil", "demon", "hell", "zombie", "bloodlust", "abhorring", "brutal", "choking", "corpse", "decapitated", "suicide", "vampire", "mutilation",  "horror", "horror movies", "horror movie", "scary movies", "scary movie", "murdur"];
var broad_queries = ["movie", "movies", "apps", "app", "book", "books", "game", "games", "crafting", "art"];

var dict_initialized = false;
var curse_dict = {};
var sexseeking_dict = {};
var lgbt_dict = {};
var horror_dict = {};
var broadphrase_dict = {};
var entity_name2icon = {};
var entity_icon2name = {};
var andQuery_entities = [];

var context_entities = [];
var result_entities = [];
var cur_context_class = "#context_entities";
var cur_srp_class = ".srp_content";
var cur_querybox = ".querybox";

// Prevent multiple touch/click causing echo. If we get click event for same entity within 3 sec, ignore later ones.
var last_clicked_entity = "";
function clearLastClickedEntity() {
  last_clicked_entity = "";
}

function contextSwitchTo(ui) {
  // ui:0 knowledge graph UI
  // ui:1 video search UI
  // ui:2 challenge UI, not implemented yet
  // ui:3 watch page UI
  ui_status = ui;
  if (ui == 3) {
      cur_querybox = ".refinement_query";
      cur_context_class = ".refinements";
      cur_srp_class = ".relatedbar";
      $(".fullwidth").hide();
      $("#watchpage").show();
  } else {
      $(".iframe").html("");
      andQuery_entities.splice(1, 1);
      cur_querybox = ".querybox";
      cur_context_class = "#context_entities";
      cur_srp_class = ".srp_content";
  }
  if (ui < 2) { 
        $("#watchpage").hide();
        $(".fullwidth").show();
        $("ul").find("li").attr("class", "");
        $("ul").find("li").eq(ui).attr("class", "active");
        if (ui == 0) {
              $("#content").hide();
	      $("#general_entities").show();
        } else {
	      $("#general_entities").hide();
              $("#content").show();
        }
  }
}

function curUIStatus() {
  return ui_status;
}

function curUI() {
  return vertical_name[ui_status];
}

function curContextEntityClass() {
  return cur_context_class;
}

function curSrpClass() {
  return cur_srp_class;
}

function curQueryBoxClass() {
  return cur_querybox;
}

var general_entities_style_backup;

function fillingBadWordsDict() {
        if (dict_initialized) {
          return;
        }
	for (var x = 0; x < curse_words.length; x++) {    
	    curse_dict[curse_words[x]] = true;
	}
	for (var x = 0; x < sex_seeking.length; x++) {    
	    sexseeking_dict[sex_seeking[x]] = true;
	}
	for (var x = 0; x < lgbt_words.length; x++) {    
	    lgbt_dict[lgbt_words[x]] = true;
	}
	for (var x = 0; x < horror_phrases.length; x++) {    
	    horror_dict[horror_phrases[x]] = true;
	}
	for (var x = 0; x < broad_queries.length; x++) {    
	    broadphrase_dict[broad_queries[x]] = true;
	}
        dict_initialized = true;
}

// -1: nothing wrong 0: empty query, 1: too broad 2: bad manner 3: sex related 4: horror 
function checkQueryPhrase(query) {
  var terms = query.split(" ");
  if (terms == "") {
    return 0;
  }
  if (terms.length == 1 && broadphrase_dict[query]) {
    return 1;
  }
  for (var x = 0; x < terms.length; x++) {    
          var idx = x;
          var term = terms[x];
          var bigram = "";
          if (idx < terms.length - 1) {
            bigram = term + " " + terms[(idx + 1).toString()];
          }
	  if (curse_dict[term] || curse_dict[bigram]) {
	    return 2;
	  } 
	  if (sexseeking_dict[term] || sexseeking_dict[bigram]) {
	    return 3;
	  } 
	  if (lgbt_dict[term] || lgbt_dict[bigram]) {
	    return 3;
	  }
	  if (horror_dict[term] || horror_dict[bigram]) {
	    return 4;
	  }
  }

  return -1;
}

function convertBadWord(query) {
  query = query.replace(/\s+/g,' ');
  var query_class = checkQueryPhrase(query);
  switch (query_class) {
    case -1:
        return query;
    case 0:
        return deft_query;
    case 1:
	return query + " for kids";
    case 2:
        return "teaching children manners";
    case 3:
        return "gender education for kids";
    case 4:
        return "holloween kids";
  }
}

//user query entity in format of <icon>(entity_name)
// notice icon can be empty if user gives us a query string which do not match any entity in DB
function entityNameFromUserQueryEntity(entity) {
      var name_start = entity.indexOf("(");
      var name_end = entity.indexOf(")");
      if (name_start < 0 || name_end < 0) {
        return "";
      }
      return entity.slice(name_start + 1, name_end);
}
function iconFromUserQueryEntity(entity) {
      var name_start = entity.indexOf("(");
      var name_end = entity.indexOf(")");
      if (name_start < 0 || name_end < 0) {
        return "";
      }
      return entity.slice(0, name_start);
}

function composeUserQuery() {
    var query = "";
    for (var x = 0; x < andQuery_entities.length; x++) {    
      var entity_name = entityNameFromUserQueryEntity(andQuery_entities[x]);
      if (entity_name == "") {
        continue;
      }
      if (query !== "" ) {
        query += " AND ";
      }
      query += entity_name;
    }
    return query;
}

function fillInLoggingParams() {
  var logging_params = {
    "ip" : myip,
    "query" : composeUserQuery(),
    "referrer" :  referrer,
    "lang" :  $(".lang_selection").val(),
    "vertical" : curUI(),
  }
  return logging_params;
}

// In CGI, developer can put "?dev=<php_file>,<...>" to switch to a development version of php code.
function ToggleDevPhp(php_file) {
      if (devphp_set[php_file]) {
        php_file += ('-' + devphp_set[php_file]);
      }
      php_file += ".php";
      return php_file;
}

// Parse CGI parameters "?key=value&..."
function getUrlParameter() {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    devphp_set = {};
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === "deb") {
            deb_level = sParameterName[1] === undefined ? 0 : sParameterName[1];
        } if (sParameterName[0] === "uw") {
            url_weight = sParameterName[1] === undefined ? 5 : parseInt(sParameterName[1]);
        } if (sParameterName[0] === "tw") {
            title_weight = sParameterName[1] === undefined ? 4 : parseInt(sParameterName[1]);
        } if (sParameterName[0] === "sw") {
            snippet_weight = sParameterName[1] === undefined ? 2 : parseInt(sParameterName[1]);
        } if (sParameterName[0] === "smooth") {
            smooth = sParameterName[1] === undefined ? 40 : parseInt(sParameterName[1]);
        } if (sParameterName[0] === "readdb") {
            readdb = sParameterName[1] === undefined ? 1 : parseInt(sParameterName[1]);
        } else if (sParameterName[0].indexOf("dev") == 0) {
            var dev_php = sParameterName[1] === undefined ? "" : sParameterName[1];
	    var phps = dev_php.split(",");
	    for (var x = 0; x < phps.length; x++) {    
		    devphp_set[phps[x]] = sParameterName[0];
            }
        } else if (sParameterName[0] === "context") {
            context = sParameterName[1] === undefined ? "" : sParameterName[1];
        } else if (sParameterName[0] === "q") {
            cgi_q = sParameterName[1] === undefined ? "" : sParameterName[1];
        } else if (sParameterName[0] === "cse") {
            if (sParameterName[1] !== undefined) {
               if (sParameterName[1].indexOf(":") > 0) {
		    cse_cx = sParameterName[1];
               } else {
		    cse_cx = cse_prefix + sParameterName[1];
               }
            }
        }
    }
}

// Compute an icon html based on icon filename and entity name. Given entity name may be in non-En, it does not have a 1:1 match against icon filename
function entityHtmlFromToken(icon, name) {
     var dot_pos = icon.indexOf(".");
     var prefix = icon.substring(0, dot_pos); 
     has_voice_icons = true;
     var image =  "entity_icons/" + icon;
     var lang = $(".lang_selection").val();
     var audio_file  = "entity_mp3/" + lang + "/" + prefix + "_" + lang + ".mp3";
     var suggestion = '<button class="entity_icon_button" audio="' + audio_file + '" title="' + name + '">';
     suggestion += '<img class="entity_icon_img" src="' + image + '">';
     suggestion += '</button>';
     return suggestion;
}

function handleCseDBResponse(response) {
   var pos_map = response.hasOwnProperty("pos") ? parseInt(response["pos"].trim()) : -1;
   var url = response.hasOwnProperty("url") ? response["url"].trim() : "";
   var title = response.hasOwnProperty("title") ? response["title"].trim() : "";
   var snippet = response.hasOwnProperty("snippet") ? response["snippet"].trim() : "";

   if (!response.hasOwnProperty("q") || !response.hasOwnProperty("pos_map")) {
     return;
   }
  
   var query = response["q"];
   var pos_map = response["pos_map"];
   entity_result_map[query] = pos_map;
}
    
function preprocessTermMap() {
    var done  = false;
    if (total_cse_responses > 0.95 * total_entities) {
        done = true;
    }
    if (!done) {
      alert("Wait a bit, total_entities:" + total_entities.toString() + " finished:" + total_cse_responses.toString());
      return;
    }
   for (query in entity_result_map) {
           var pos_map = entity_result_map[query];
           var term_map = {};
	   for (pos in pos_map) {
                   var uw = parseInt($(".uw_selection").val());
                   var tw = parseInt($(".tw_selection").val());
                   var sw = parseInt($(".sw_selection").val());
                   var sl = parseInt($(".smooth_selection").val());
		   var pos_weight = positionWeight(pos, smooth);
		   var title_terms = pos_map[pos].title.split(" ");
		   var snippet_terms = pos_map[pos].snippet.split(" ");
		   var url_terms = pos_map[pos].url.split(" ");
		   readFeatureIntoTermMap(url_terms, uw, pos_weight, term_map);
		   readFeatureIntoTermMap(title_terms, tw, pos_weight, term_map);
		   readFeatureIntoTermMap(snippet_terms, sw, pos_weight, term_map);
            }
           var vec = composeWeightSortedTermVec(term_map);
   	   var new_map = composeTermMapFromTermVec(vec, 100);
	   entity_term_map[query] = new_map;
   }
   alert("ALL DONE");
}

$("body").on('click', '.oPenButton', function() {
   preprocessTermMap();
});

function handleEntityRequestResponse(response) {
   if (response.hasOwnProperty("ip")) {
	    myip = response["ip"];
   }
   var context_suggestion = response.hasOwnProperty("context") ? response["context"].trim() : "";
   var general_suggestion = response.hasOwnProperty("general") ? response["general"].trim() : "";
   var watch_history_suggestion = response.hasOwnProperty("watch_history") ? response["watch_history"].trim() : "";
   var entity_history_suggestion = response.hasOwnProperty("entity_history") ? response["entity_history"].trim() : "";

   if (context_suggestion != "") {
	   $(curContextEntityClass()).html("");
	   var context_tokens = context_suggestion.split(",");
	   for (var x = 0; x < context_tokens.length; x++) {
	     var token = context_tokens[x];
	     var entity_icon = token;
	     // ename_in_ulang : entity name in user language
	     var ename_in_ulang = "";
	     if (token.indexOf('(') >= 0) {
	       entity_icon = token.slice(0, token.indexOf('('));
	       ename_in_ulang =  token.slice(token.indexOf("(") + 1, -1);
	     }
	     if (!entity_icon || context_entities.hasOwnProperty(entity_icon)) {
	       continue;
	     }
	     entity_name2icon[ename_in_ulang] = entity_icon;
	     entity_icon2name[entity_icon] = ename_in_ulang;
	     context_entities.push(entity_icon);
	     $(curContextEntityClass()).append(entityHtmlFromToken(entity_icon, ename_in_ulang));
	   }
   }
   if (general_suggestion != "") {
	   $("#general_entities").html("");
	   var tmp_html = "";
	   var per_cat_entities = general_suggestion.split("],[");
	   for (var i = 0; i < per_cat_entities.length; i++) {
		   if (per_cat_entities[i].indexOf("[") >= 0) {
		      per_cat_entities[i] = per_cat_entities[i].slice(per_cat_entities[i].indexOf("[") + 1);
		   } else if (per_cat_entities[i].indexOf("]") >= 0) {
		      per_cat_entities[i] = per_cat_entities[i].slice(0, per_cat_entities[i].indexOf("]"));
		   }
		   if (per_cat_entities[i] == "") {
		     continue;
		   }
		   // One category per row, each row is a <div>
		   tmp_html +=  "<div class='per_cat_entities'>";
		   var general_tokens = per_cat_entities[i].split(",");
		   for (var x = 0; x < general_tokens.length; x++) {
		     var token = general_tokens[x];
		     var entity_icon = token;
		     var ename_in_ulang = "";
		     if (token.indexOf('(') >= 0) {
		       entity_icon = token.slice(0, token.indexOf('('));
		       ename_in_ulang =  token.slice(token.indexOf("(") + 1, -1);
		     }
		     if (!entity_icon) {
		       continue;
		     }
                     if (entity_icon2name.hasOwnProperty(entity_icon)) {
                       continue;
                     }
		     entity_name2icon[ename_in_ulang] = entity_icon;
		     entity_icon2name[entity_icon] = ename_in_ulang;
                     total_entities++;
		     tmp_html += entityHtmlFromToken(entity_icon, ename_in_ulang);
		   }
		   tmp_html += "</div>";
	   }
	   $("#general_entities").html(tmp_html);
   }
   if (watch_history_suggestion != "") {
     watch_history = watch_history_suggestion.split(",");
   }
   if (entity_history_suggestion != "") {
     entity_history = entity_history_suggestion.split(",");
   }
	   
   // Handle entity icon click, play audio, issue a search
   $("body").on('click', '.entity_icon_button', function() {
    if (event.type == "click") documentClick = true;
    if (!documentClick) {
      return;
    }
    var entity_name = $(this).attr("title");

    if (!entity_name2icon.hasOwnProperty(entity_name)) {
      return;
    }
    // A stupid hack, mobile device somehow trigger this event multiple times within a second, cause echo.
    if (entity_name == last_clicked_entity) {
	 return;
    }
    last_clicked_entity = entity_name;
    setTimeout(clearLastClickedEntity, 3000);
    var icon = $(this).find("img").attr("src");
    icon = icon.slice(icon.indexOf("/") + 1);
  
       andQuery_entities[0] = icon + "(" + entity_name + ")";
       var audio_file = $(this).attr("audio");
       audio = document.createElement("audio");
       audio.src = audio_file;
       audio.play();
   
    var distance_map = {};
    for (dest in entity_name2icon) {
      if (entity_name == dest || !entity_term_map.hasOwnProperty(dest) || !entity_term_map.hasOwnProperty(entity_name)) {
        continue;
      }
      var cosine = cosineSimilarity(entity_term_map[dest], entity_term_map[entity_name]);
      distance_map[dest] = cosine; 
    } 
    var nearest_entities = composeWeightSortedTermVec(distance_map);
    var cutoff = Math.min(12, nearest_entities.length);

    $(curContextEntityClass()).html("");
    $("#new_neighbors").html("");
    for (var i = 0; i < cutoff; ++i) {
       var neighbor = nearest_entities[i];
       $("#new_neighbors").append(entityHtmlFromToken(entity_name2icon[neighbor.term], neighbor.term));
    }
       RequestEntitySuggestion(icon, /*query*/
			    true, /*async*/
			    myip,  /*getip */
			    "1",  /*context*/
			    "0",  /*general*/
			    "0"   /*history*/);
    
    var image_url = 'url("entity_icons/' + icon + '")';
    $(curQueryBoxClass()).css("background-image", image_url);
    $(curQueryBoxClass()).css("background-position", 'right');
    $(curQueryBoxClass()).val(entity_name);
  });
}


// Ask backend to provide entity based on  the latest query (either the letter typed so far, or the icon just clicked)
// Context: 1/0,  if 1, ask for context entities based on context ( the latest query, either the letter typed so far, or the icon just clicked)
// General: 1/0, if 1, ask entities based on user interest (ranked top)
function RequestEntitySuggestion(query, asynchronize, getip, context, general, history) {
	var php_file = ToggleDevPhp("entity_fetch");
        var request = {};
        request["getip"] = getip;
        request["q"] = query;
        request["lang"] = $(".lang_selection").val();
        request["c"] = context;
        request["g"] = general;
        request["h"] = history;
        $.ajax({
	  url: php_file,
	  dataType: 'json',
	  async: asynchronize,
	  data: request,
	  success: handleEntityRequestResponse
	});
}

function RequestCseDB(query, pos) {
	var php_file = ToggleDevPhp("read_csedb");
        var request = {};
        request["q"] = query;
        request["pos"] = pos;
        $.ajax({
	  url: php_file,
	  dataType: 'json',
	  async: true,
	  data: request,
	  success: handleCseDBResponse
	});
}


function YTSearchUrl(query, ctxt, lang, max_results) {
      query = query.replace(/\s+/g, "%20");
      var url = "https://www.googleapis.com/youtube/v3/search?";
      url += "safeSearch=strict";
      url += "&part=snippet";
      url += "&type=video";
      url += "&maxResults=" + max_results.toString();
      url += "&key=" + $("#key").val();
      url += "&q=" + query;
      if (lang == "en") {
	      if (ctxt !== "") {
		  url += "%20" + ctxt.replace(/ /g, "%20");
	      }
      } else {
        if (lang == "zh") {
          url += "&relevanceLanguage=zh-Hans";
        } else {
          url += ("&relevanceLanguage=" + lang);
        }
      }
      return url;
}

function resultsPerRow(width) { 
      // Create a temporary resultwrap to measure screen width
      $('<div>', {'class' : 'resultwrap'}).appendTo("body");
      var result_width = parseInt($(".resultwrap").css("width").slice(0, -2));
      $(".resultwrap").remove();
      // Estimate how many results we can show in one row.
      return Math.floor(width / result_width);
}

function watchHistoryHtml(results_per_row) {
        if (watch_history.length < results_per_row) {
          return "";
        }
        var out = "";
        var displayed_items = 0;
        for(var i = 0; i < watch_history.length; i++) {
	    // item contains "<ytid>;<title>;<displaylink>"
            var item = watch_history[i];
            var item_content = item.split(";");
            if (item_content.length < 3) {
              continue;
            }
            var ytid = item_content[0];
            var thumbnail = thumbnailYTLink(ytid);
            var result = new Array();
            result["ytid"] = item_content[0];
            result["title"] = item_content[1];
            result["displaylink"] = item_content[2];
            result["thumbnail"] = thumbnail;
            out += resultHtml(result, "srpresult");
            displayed_items++;
            if (displayed_items % results_per_row == 0) {
                    // Display at most two rows of watch history videos
                    if (watch_history.length - 1 - i  < results_per_row) {
                       break;
                    }
            }
        }
        return out;
}

function entityHistoryHtml() {
        var out = "";
        for(var i = 0; i < entity_history.length; i++) {
	    // item contains "icon(name)"
            var entity = entity_history[i];
	    var icon = iconFromUserQueryEntity(entity);
	    var name = entityNameFromUserQueryEntity(entity);
	    if (icon == "" || name == "") {
	       continue;
	    }
	    out += entityHtmlFromToken(icon, name);
        }
        return out;
}

function digestSearchResponse(response) {
        var out = "";
        var digest_arr = [];
        var result_ytids = [];
        // srp = [];
        digest = {};
        if (!response.items) {
          return;
        }
        var displayed_items = 0;
        for(var i = 0; i < response.items.length; i++) {
            var item = response.items[i];
            var result = parseYoutubeSearchResponse(item);
            
	    if (result.hasOwnProperty("failed")) {
		    if (deb_level && deb_level > 1) {
		      out += "<br/>" + result["debug"];
		    }
		    continue;
            }

            var rtype = (curUI() == "wp") ? "relatedres" : "srpresult";
	    out += resultHtml(result, rtype);
            // Get clean text (remove punct) to retrieve related entities (per result and overall for the query)
            // Backend will do text match b/w clean_text and entity names to compute related entities.
            var title_clean_txt = result["title"].replace(/[.,\/#!$%\^&\*;:{}=\-_`~()|\"\']/g, " ");
	    var descript_clean_txt = "";
            if (result.hasOwnProperty("description")) {  
		 descript_clean_txt = result["description"].replace(/[.,\/#!$%\^&\*;:{}=\-_`~()|\"\']/g, " ");
            }
            digest_arr.push(title_clean_txt + "|" +  descript_clean_txt);
            ++displayed_items;
            if (deb_level && deb_level > 1) {
		out += '<br/>' + result["debug"];
	    }
	    result_ytids.push(result["ytid"]);
      }
      digest["content"] = out;
      digest["result_ytids"] = result_ytids;
      digest["digest_arr"] = digest_arr;
      return;
}

var toBeScrapedP1 = [];
var scraped = [];

function scrapeNextQuery() {
   if (scraped.length % 100 == 10) {
	  alert("finished: " + scraped.length.toString() + ", total_cse_responses:" + total_cse_responses.toString());
   } 
   if (toBeScrapedP1.length > 0) {
      var query = toBeScrapedP1.shift();
      GoogleCSESearch(query, 1);
      scraped.push(query);
      setTimeout(scrapeNextQuery, 300);
   } else { 
	   alert("ALL DONE! finished: " + scraped.length.toString() + ", total_cse_responses:" + total_cse_responses.toString());
   }
}

function composeHomepage() {
      var cnt = 0;
      alert("Start to scrape " + total_entities.toString() + " queries. " + "map size:" + Object.keys(entity_name2icon).length.toString() + ", press to continue!");
      if (readdb == 1) {
       for (entity in entity_name2icon) {
         RequestCseDB(entity);
         total_cse_responses++;
        } 
	alert("ALL FETCHED! total_cse_responses:" + total_cse_responses.toString());
      } else {
         for (entity in entity_name2icon) {
           toBeScrapedP1.push(entity);
         }
         scrapeNextQuery();
      }
}

function Search(query) {
      if (query != "") {
	  $(".watch_history").html("");
	  $(".watch_history").hide();
	  $(".entity_history").html("");
	  $(".entity_history").hide();
      }
      query = convertBadWord(query);
          
      var lang = $(".lang_selection").val();
      var ctxt = (context === undefined) ? "for kids" : context;
      var url = YTSearchUrl(query, ctxt, lang, 50);
      var content_html = "";
      if (deb_level && deb_level > 0) {
        content_html += "<b>" + url + "</b><br/>";
      }
      $.get(url, handleSearchResponse);
      var logging_params = fillInLoggingParams();
      var php_file = ToggleDevPhp("logging");
      $.post(php_file, logging_params);
}

function logImpressions(digest) {
      var logging_params = fillInLoggingParams();
      logging_params["etype"] = 'im';
      logging_params["impressions"] = digest["result_ytids"].join(",");
      var php_file = ToggleDevPhp("logging");
      $.post(php_file, logging_params);
}


function handleSearchResponse(response, status) {
	digestSearchResponse(response);
        $(curSrpClass()).html(digest["content"]);
}

$(document).ready(function() {
        // This should absolutely be the first line of code!!!
        getUrlParameter();

	srp_results_per_row = resultsPerRow($(window).width());
        var getip = "1";
        if (baseURI.indexOf("cse.html") >= 0) {
          getip = "developer";
        }
        RequestEntitySuggestion("" /*query*/,
                                false /*async*/,
                                getip,
                                0 /*context*/,
                                1 /*general*/,
                                0 /*history*/);
        general_entities_style_backup = $("#general_entities").cssAll('position,top,left,bottom,border-style,border,margin,padding,overflow');
        // cgi_q is for debuging or scraping for analysis
        fillingBadWordsDict();
        fillingStopWordsDict();
        contextSwitchTo(1);
        composeHomepage();
	andQuery_entities[0] = "(video)";

    // Switch tabs 
    $(".tablinks").on('click touchend', function(){
      var idx = $(this).index(".tablinks");
      if (curUIStatus() == idx) {
        return;
      }
      contextSwitchTo(idx);
      if (curUIStatus() == 1) {
         var cnt = 0;
         for (src in entity_name2icon) {
	    var distance_map = {};
	    for (dest in entity_name2icon) {
	      if (src == dest || !entity_term_map.hasOwnProperty(dest) || !entity_term_map.hasOwnProperty(src)) {
		continue;
	      }
	      var cosine = cosineSimilarity(entity_term_map[src], entity_term_map[dest]);
	      distance_map[dest] = cosine; 
	    } 
	    var nearest_entities = composeWeightSortedTermVec(distance_map);
	    var cutoff = Math.min(16, nearest_entities.length);
	    for (var i = 0; i < cutoff; ++i) {
	       var neighbor = nearest_entities[i];
	       var logging_params = {
	          "table" : "entity_neighbors",
	          "query" : src,
	          "neighbor" :  neighbor.term,
	          "cosine" :  neighbor.weight,
	       }
	       var php_file = ToggleDevPhp("cse_logging");
	       $.post(php_file, logging_params);
            }
            cnt++;
            if (cnt % 100 == 10) {
		    alert("finished: " + cnt.toString());
            }
	 }
         alert("ALL DONE: " + cnt.toString());
      }
    });

    // Click anywhere on the top bar to close watchpage
    $("body").on('click touchend', '.back_to_kg', function(){
        contextSwitchTo(0);
	return;
    });

    // Click anywhere on the top bar to close watchpage
    $("body").on('click touchend', '.back_to_hp', function(){
        contextSwitchTo(1);
	return;
    });

	$("body").on('focus', '.querybox, .refinement_query', function(e) {
          e.preventDefault();
	  $(this).css("background-image", "none");
          $(this).val("");
          
          var cur_class = $(this).attr("class");
          if (cur_class == "querybox") {
		andQuery_entities[0] = "(video)";
          } else {
	        andQuery_entities.splice(1, 1);
          }
        });

	$("body").on('keyup', '.querybox, .refinement_query', function(e) {
            var event = e || window.event;
            var enter_pressed = false;
            var start_search = false;
	    if (  event.type == "keyup" ) {
	        var charCode = event.which || event.keyCode;
	        if (charCode == 13 ) {
                    enter_pressed = true;	
            	    start_search = true;
		    preprocessTermMap();
                    return;
                }
            }

            var cur_class = $(this).attr("class");
            var query_entity_idx = cur_class == "refinement_query" ? 1 : 0;

	    if (!enter_pressed) {
	      var query = $(this).val();
              if (!query) {
                return;
              }
              if (entity_name2icon.hasOwnProperty(query)) {
                 andQuery_entities[query_entity_idx] = entity_name2icon[$(this).val()] +  "(" + $(this).val() + ")";
		 var image = 'url("entity_icons/' + entity_name2icon[query] + '")';
		 $(this).css("background-image", image);
		 $(this).css("background-position", 'right');
                 start_search = true;
              } else {
                 RequestEntitySuggestion(query,
                                         true, /*async*/
                                         myip,
                                         "0", /*context*/
                                         "0", /*general*/
                                         "0"  /*history*/);
              }
            }
	    if ( start_search) {
	      $(this).blur();
              if (curUI() == "wp") {
		  $(".watchpage_topbar").html(composeWatchpageTopBar());
              } else {
                  contextSwitchTo(1);
              }
	      try {
		  Search(composeUserQuery());
	      }
	      catch (err) {
		  var out =  "<b>  Oops, nothing found here </b><br/><br/>";
         	  $(curSrpClass()).html(out);
	      }
            }
	});
        
        // Handle result click, show watchpage window
        $("body").on('click', '.resultwrap', function(){
	     handleVideoClick($(this));
        });

        // Whenever user switch language,we should do a Request entity suggestion for two reasons
        // A: Some entities does not have translation in user pick language they should not be shown
        // B: Retrieve entity name in user language for search.
	$(".lang_selection").change(function() {
	  RequestEntitySuggestion("",
	  			  true, /*async*/
				  myip,
				  "0", /*context*/
				  "1", /*general*/
				  "0"  /*history*/);
	});
});


