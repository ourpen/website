var user_skills = {};
var no_iframe_detect = 1;
var deb_level;
var devphp_set = {};
var rec = -1; 
var context; 
var current_page = 0;
var cgi_q = "";
var documentClick = false;
var watch_history = [];
var referrer =  document.referrer;
var baseURI =  document.baseURI;
var documentURI =  document.documentURI;


// According to some stackflow page, this can prevent unintentional touchmove become a click event
$(document).on('touchstart', function() {
    documentClick = true;
});
$(document).on('touchmove', function() {
    documentClick = false;
});

// A util to retrieve and set a list off css property
(function($)
{
    $.fn.cssAll = function(css)
    {
        var obj = {};

        if(this.length)
        {
            var css = css.split(',');
            var params = [];

            for(var i=0,ii=css.length; i<ii; i++)
            {
                params = css[i].split(':');

                obj[$.trim(params[0])] = $(this).css($.trim(params[1] || params[0]));
            }
        }

        return obj;
    };
})(jQuery);


// Parse YouTube search data API, response structure is different than google CSE API
function parseYoutubeSearchResponse(item) {
  var result = {};
  var title = item.snippet.title;
  if (title.length > 43) {
	  title = title.slice(0, 40) + "...";
  }
  result["title"] = title;
  result["thumbnail"] = item.snippet.thumbnails.medium.url;
  result["description"] =  item.snippet.description;
  result["ytid"] = item.id.videoId;
  result["url"] = "https://www.youtube.com/watch?v=" + item.id.videoId;
  var channelTitle = item.snippet.channelTitle;
  if (channelTitle.length > 24) {
    channelTitle = channelTitle.slice(0, 21) + "...";
  }
  result["displayLink"] = channelTitle;
  if (deb_level && deb_level > 1) {
    result["debug"] = JSON.stringify(item);
  }
  if (!result.hasOwnProperty("url") || !result.hasOwnProperty("title") || !result.hasOwnProperty("thumbnail") || !result.hasOwnProperty("displayLink")) {
    result["failed"] = true;
    if (deb_level && deb_level > 1) {
	result["debug"] += '<br/><b>Something is missing!</b><br/>';
    }
  }
  return result;
}

// Parse google CSE json response
function parseGoogleSearchResponse(item) {
    var result = {};
    if (deb_level && deb_level > 1) {
	    result["debug"] = JSON.stringify(item);
    }
    if ( !item.pagemap) {
      result["failed"] = true;
      if (deb_level && deb_level > 1) {
	result["debug"] += '<br/><b>No pagemap!</b><br/>';
      }
      return result;
    }
    result["displayLink"] = item.displayLink;
    var title = item.title;
    if (title.length > 48) {
	  title = title.slice(0, 48) + "...";
    }  
    result["title"] = title;
    result["url"] = item.link;

    // Consider remove this logic for price and duration. Not being used.
    if ( item.pagemap.offer && item.pagemap.offer[0].hasOwnProperty("price") &&
	  item.pagemap.offer[0].hasOwnProperty("pricecurrency") && item.pagemap.offer[0]["pricecurrency"] == "USD" ) {
      result["price"] = item.pagemap.offer[0]["price"];
    }
    if ( item.pagemap && item.pagemap.metatags && item.pagemap.metatags[0].hasOwnProperty("og:description")) {
      result["description"] = item.pagemap.metatags[0]["og:description"];
    }
    if ( item.pagemap.videoobject && item.pagemap.videoobject[0].hasOwnProperty("duration")) {
      duration = item.pagemap.videoobject[0]["duration"];
      duration = duration.replace(/PT/, '');
      duration = duration.replace(/M/, ':');
      duration = duration.replace(/S/, '');
      var minutes = parseInt(duration.substring(0, duration.indexOf(":")));
      var seconds = parseInt(duration.substring(duration.indexOf(":") + 1));
      var hours;
      if (minutes >= 60) {
	hours = Math.floor(minutes/60);
	minutes %= 60;
      }
      duration = ""; 
      if (hours) {
	duration += hours.toString() + ":";
      } 
      duration += minutes.toString() + ":";
      duration += seconds.toString();
      result["duration"] = duration;
    }
    if ( item.pagemap.imageobject) {
      result["thumbnail"] = item.pagemap.imageobject[0].url;
    } else if ( item.pagemap.cse_image ) {
      result["thumbnail"] = item.pagemap.cse_image[0].src;
    } 
   
    if (!result.hasOwnProperty("url") || !result.hasOwnProperty("title") || !result.hasOwnProperty("thumbnail") || !result.hasOwnProperty("displayLink")) {
      result["failed"] = true;
      if (deb_level && deb_level > 1) {
	result["debug"] += '<br/><b>Something is missing!</b><br/>';
      }
      return result;
    }
    var url = result["url"];
    if (url.indexOf("youtube") > 0) {
      if (url.indexOf("watch") > 0) {
	      var start = url.indexOf("v=") + 2;
	      var cuthead = url.slice(start);
	      var end = cuthead.indexOf("&");
	      if (end < 0) { 
		result["ytid"] = cuthead;
	      } else {
		result["ytid"] = cuthead.slice(0, end);
	      }
      } else if (url.indexOf("playlist") > 0) {
	      var start = url.indexOf("list=") + 5;
	      var cuthead = url.slice(start);
	      var end = cuthead.indexOf("&");
	      if (end < 0) { 
		result["playlistid"] = cuthead;
	      } else {
		result["playlistid"] = cuthead.slice(0, end);
	      }
      }
   }
   return result;
}

// Get user query
// TODO: we should enable a text query box, so user can type letters and recommend icons with entity name prefix  matching user input 
// once user click the icon, we replace the prefix string with the entity name.
function userQuery() {
  return $(".querybox").val().replace(/\s+/g,' ').trim().toLowerCase();
}


// Recording dialog, disabled, consider to turn it on to allow user edit the TTS voice and more
// This is a social feature to encourage participation and crowd wisdom to improve our content.
$(function() {
    $( "#recording-dialog" ).dialog({
       autoOpen: false, 
       buttons: {
	  OK: function() {$(this).dialog("close");}
       },
       position: {
	  my: "left center",
	  at: "left center"
       }
    });
});

// This logic is now disabled, earlier I allow google CSE return any website, but some
// block iframe, this function detect such blocker and turn the snippet back to href link
// Now I only let google CSE return website manually verified to be friendly with iframe.
// https://www.whitehatsec.com/blog/x-frame-options-xfo-detection-from-javascript/
// See google CSE config here: https://cse.google.com/cse/setup/basic?cx=014484386212996657006%3A-hqy9dxsm08
// Note video use YouTube data API instead of CSE
function detectIframeBlocker(urls) {
	dojo.forEach(urls, function(url) {
		var iframe = dojo.create("iframe", { src: url, id: url });
		dojo.attr(iframe, "style", {display: 'none'});
		dojo.attr(iframe, "sandbox", "allow-forms allow-same-origin allow-scripts");
		dojo.connect(iframe, "onload", function() {
			dojo.destroy(iframe);
		});
		dojo.place(iframe, dojo.body());
		setTimeout(function () {
			var obj = dojo.byId(url);
			if (obj) {
				dojo.destroy(iframe);
			} else {
                                var selector = "[linkdata='" + url + "']";
				var image = $(selector).attr("imagedata");
                                var href = '<a href="' + url + '"><img src="' + image + '" class="result_thumbnail"></a>';
				$(selector).html(href);
			        $(selector).attr("linkdata", "");
			      var iframe_blocker = {"url" : url};
			      var php_file = ToggleDevPhp("iframe_blocker_logging");
			      $.post(php_file, iframe_blocker);
			}
		}, 5000);
	});
}

var isIOS = ((/iphone|ipad/gi).test(navigator.appVersion));
var ui_status = 0; // 0: entity selection; 1: result page; 2: iframe;
var speaker = 1;  // 1: On 0:Off
var recorder = 0;  // 1: On 0:Off
var active_rightcol = -1;
var active_vertical = 1;
// See google CSE config here: https://cse.google.com/cse/setup/basic?cx=014484386212996657006%3A-hqy9dxsm08
var cse_cx = [
             //  "014484386212996657006:lsiv75baefc",  // web
             // "014484386212996657006:s_8ii2n3ih0",  // craft & art
              "",                                   // knowledge base 
              "014484386212996657006:-hqy9dxsm08",  // video
              //  "014484386212996657006:ryndze0bw8c",  // games
             //  "014484386212996657006:veuenq0cwcc",  // book
              // "014484386212996657006:qlryzlxvz84"  // toy
              ]; 
// Initials for each vertical, "k": knowledge entities. v:video, g:game, b:book
var cse_name = ["k", "v",  "g", "b"];
var video_vertical = 1;
// Common prefix in case we want to debug a new CSE with different suffix
var cse_prefix = "014484386212996657006:";

var curse_words  = ["bull shit", "ass hole", "fuck", "fucks", "fucked", "fucking", "fucker", "anus",  "bitch", "asshole", "sucker", "damn", "fuckass", "motherfucker", "pussy", "shit", "idiot", "nigro", "niggers", "butt", "jackass", "testicle", "whore"];
var sex_seeking = ["xxx", "porn", "nake", "nak girl", "nud", "nak", "naked", "nude", "nudity", "blow job", "sex", "dick", "butt", "genital", "make love", "intercourse", "cock", "handjob", "humping", "penis", "vagina"];
var lgbt_words = ["gay", "transgender", "transgender", "homosexual", "lesbian", "queer", "asexual"];
var horror_phrases = ["ghost", "torture", "devil", "demon", "hell", "zombie", "bloodlust", "abhorring", "brutal", "choking", "corpse", "decapitated", "suicide", "vampire", "mutilation",  "horror", "horror movies", "horror movie", "scary movies", "scary movie", "murdur"];
var broad_queries = ["movie", "movies", "apps", "app", "book", "books", "game", "games", "crafting", "art"];

var dict_initialized = false;
var curse_dict = {};
var sexseeking_dict = {};
var lgbt_dict = {};
var horror_dict = {};
var broadphrase_dict = {};
var entity_name2icon = {};

// Entities detected in result set. One item per result. Each item contains a space separated list of icon filenames.
// Currently not being used. 
var result_entities = [];

// Design a voice remind for each function for new user, now turned off.
function searchRemind() {
  skillRemind("search", 3);
}

// Earlier use a Check button on right side to close SRP, now use a knowledge vertical icon instead.
function closeContent() {
  skillRemind("closecontent", 3);
}

// Prevent multiple touch/click causing echo. If we get click event for same entity within 3 sec, ignore later ones.
var last_clicked_entity = "";
function clearLastClickedEntity() {
  last_clicked_entity = "";
}


// Disabled, design an animated finger touch to remind user how to use oPen!
var skill_map = {'search' : 'oPenButton', 'closecontent' : '#closeContent'};
function skillRemind(skill, max_remind) {
  if (user_skills[skill] < max_remind) {
	  $(skill_map[skill]).css({'opacity':'0.2'});
	  var src_top = Math.floor($(skill_map[skill]).offset().top).toString() + 'px';
	  var src_left = Math.floor($(skill_map[skill]).offset().left - 100).toString() + 'px';
	  $('.touchIcon').css({'top': src_top, 'left': src_left});
	  $('.touchIcon').show();
	  var dest_top = Math.floor($(skill_map[skill]).offset().top + $(skill_map[skill]).height() * 0.3);
	  var dest_left = Math.floor($(skill_map[skill]).offset().left + $(skill_map[skill]).width() * 0.3);
	  $('.touchIcon').animate({'top': dest_top.toString() + 'px', 'left': dest_left.toString() + 'px', 'z-index':'99999', 'opacity': '0.8'},
				   'slow',
				    function(){
				      $('.touchIcon').hide();
				      $(skill_map[skill]).css({'opacity':'1.0'});
                                      if (skill == "search") {
			                Search(userQuery());
                                      } else if (skill == "closecontent") {
                                        ForceCloseContent();
                                      }
				    });
  }
}

// Mysql DB to record whether user learned how to use each feature, once they used K times, stop audio remind, disabled.
function skillUpdate(skills, max_practice) {
  var skill_update = {};
  skill_update["id"] = user_skills["id"];
  var need_update = false;
  for (var i = 0; i < skills.length; i++) {
    var skill = skills[i];
    if (user_skills[skill] < max_practice) {
      need_update = true;
    }
    user_skills[skill] += 1;
    skill_update[skill] = user_skills[skill];
  }
  if (need_update) {
	var php_file = ToggleDevPhp("skill_update");
	$.post(php_file, skill_update);
  }
}

function ForceCloseContent() {
	$("#content").hide();
	$("#general_entities").show();
        $('#general_entities').css(general_entities_style_backup);
        ui_status = 0;
}

function CloseContent() {
	skillUpdate(["closecontent"],  3);
        ForceCloseContent();
}

var qentities_style_backup;
var querybox_style_backup;
var general_entities_style_backup;
var qicon_style_backup;
var delicon_style_backup;
var h2_display_backup;

// Close iframe result visit, show SRP again.
function CloseIframe() {
        var openbutton_styles = { 'height':'80px'};
          
	$("#iframe").html("");
        $('#q_entities').css(qentities_style_backup);
        $('.oPenButton').css({'height': qentities_style_backup["height"]});
        $('#oPenButtonImg').css({'height': qentities_style_backup["height"]});
        $('.querybox').css(querybox_style_backup);
	if (h2_display_backup != "none") {
	   $("h2").show();
        }
	$("#general_entities").hide();
        $("#verticals").show();
	$("#context_entities").show();
	// $("#closeContent").show();
	$("#content").show();
        ui_status = 1;
	skillUpdate(["closepageframe"],  3);
}

function fillingBadWordsDict() {
        if (dict_initialized) {
          return;
        }
	for (var x = 0; x < curse_words.length; x++) {    
	    curse_dict[curse_words[x]] = true;
	}
	for (var x = 0; x < sex_seeking.length; x++) {    
	    sexseeking_dict[sex_seeking[x]] = true;
	}
	for (var x = 0; x < lgbt_words.length; x++) {    
	    lgbt_dict[lgbt_words[x]] = true;
	}
	for (var x = 0; x < horror_phrases.length; x++) {    
	    horror_dict[horror_phrases[x]] = true;
	}
	for (var x = 0; x < broad_queries.length; x++) {    
	    broadphrase_dict[broad_queries[x]] = true;
	}
        dict_initialized = true;
}

// -1: nothing wrong 0: empty query, 1: too broad 2: bad manner 3: sex related 4: horror 
function checkQueryPhrase(query) {
  var terms = query.split(" ");
  if (terms == "") {
    return 0;
  }
  if (terms.length == 1 && broadphrase_dict[query]) {
    return 1;
  }
  for (var x = 0; x < terms.length; x++) {    
          var idx = x;
          var term = terms[x];
          var bigram = "";
          if (idx < terms.length - 1) {
            bigram = term + " " + terms[(idx + 1).toString()];
          }
	  if (curse_dict[term] || curse_dict[bigram]) {
	    return 2;
	  } 
	  if (sexseeking_dict[term] || sexseeking_dict[bigram]) {
	    return 3;
	  } 
	  if (lgbt_dict[term] || lgbt_dict[bigram]) {
	    return 3;
	  }
	  if (horror_dict[term] || horror_dict[bigram]) {
	    return 4;
	  }
  }

  return -1;
}

function convertBadWord(query) {
  query = query.replace(/\s+/g,' ');
  var query_class = checkQueryPhrase(query);
  switch (query_class) {
    case -1:
        return query;
    case 0:
        return deft_query;
    case 1:
	return query + " for kids";
    case 2:
        return "teaching children manners";
    case 3:
        return "gender education for kids";
    case 4:
        return "holloween kids";
  }
}

function fillInLoggingParams() {
  var logging_params = {
    "ip" : user_skills["id"],
    "gender" : $("#gender").val(),
    "age" : $("#age").val(),
    "query" : userQuery(),
    "referrer" :  referrer,
    "lang" :  $(".lang_selection").val(),
    "vertical" : cse_name[active_vertical], 
  }
  if (cgi_q) {
    logging_params["query"] = cgi_q;
    logging_params["qtype"] = "s";
  }
  return logging_params;
}

// In CGI, developer can put "?dev=<php_file>,<...>" to switch to a development version of php code.
function ToggleDevPhp(php_file) {
      if (devphp_set[php_file]) {
        php_file += "-dev";
      }
      php_file += ".php";
      return php_file;
}

// TODO: Is this expensive? Should we use string regex matching.
function url_domain(data) {
  var    a  = document.createElement('a');
         a.href = data;
  return a.hostname;
}

// Parse CGI parameters "?key=value&..."
function getUrlParameter() {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    devphp_set = {};
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === "deb") {
            deb_level = sParameterName[1] === undefined ? 0 : sParameterName[1];
        } else if (sParameterName[0] === "dev") {
            var dev_php = sParameterName[1] === undefined ? "" : sParameterName[1];
	    var phps = dev_php.split(",");
	    for (var x = 0; x < phps.length; x++) {    
		    devphp_set[phps[x]] = true;
            }
        } else if (sParameterName[0] === "rec") {
            rec = 0;
        } else if (sParameterName[0] === "context") {
            context = sParameterName[1] === undefined ? "" : sParameterName[1];
        } else if (sParameterName[0] === "q") {
            cgi_q = sParameterName[1] === undefined ? "" : sParameterName[1];
        } else if (sParameterName[0] === "nd") {
            no_iframe_detect = sParameterName[1] === undefined ? 1 : sParameterName[1];
        } else if (sParameterName[0] === "v") {
            $("li").eq(active_vertical).attr("class", "");
            active_vertical = sParameterName[1] === undefined ? 1 : sParameterName[1];
            $("li").eq(active_vertical).attr("class", "active");
        } else if (sParameterName[0] === "cse") {
            if (sParameterName[1] !== undefined) {
               if (sParameterName[1].indexOf(":") > 0) {
		    cse_cx[active_vertical] = sParameterName[1];
               } else {
		    cse_cx[active_vertical] = cse_prefix + sParameterName[1];
               }
            }
        }
    }
}

// Compute an icon html based on icon filename and entity name. Given entity name may be in non-En, it does not have a 1:1 match against icon filename
function entityHtmlFromToken(icon, name) {
     var dot_pos = icon.indexOf(".");
     var prefix = icon.substring(0, dot_pos); 
     has_voice_icons = true;
     var image =  "entity_icons/" + icon;
     var lang = $(".lang_selection").val();
     var audio_file  = "entity_mp3/" + lang + "/" + prefix + "_" + lang + ".mp3";
     var suggestion = '<button class="entity_icon_button" audio="' + audio_file + '" title="' + name + '">';
     suggestion += '<img class="entity_icon_img" src="' + image + '">';
     suggestion += '</button>';
     return suggestion;
}

// Compose context entity row (right below vertical tabs, above general entities)
// and general entities. Each row is for one category.
function setEntitySuggesions(response) {
}

// Ask backend to provide entity based on  the latest query (either the letter typed so far, or the icon just clicked)
// Context: 1/0,  if 1, ask for context entities based on context ( the latest query, either the letter typed so far, or the icon just clicked)
// General: 1/0, if 1, ask entities based on user interest (ranked top)
function RequestEntitySuggestion(query, asynchronize, getip, context, general, history) {
	var php_file = ToggleDevPhp("entity_suggestion");
        var request = {};
        request["getip"] = getip;
        request["q"] = query;
        request["lang"] = $(".lang_selection").val();
        request["c"] = context;
        request["g"] = general;
        request["h"] = history;
        $.ajax({
	  url: php_file,
	  dataType: 'json',
	  async: asynchronize,
	  data: request,
	  success: function(response) {
		   if (response.hasOwnProperty("ip")) {
			    user_skills["id"] = response["ip"];
		   }
		   var context_suggestion = response.hasOwnProperty("context") ? response["context"].trim() : "";
		   var general_suggestion = response.hasOwnProperty("general") ? response["general"].trim() : "";
		   var history_suggestion = response.hasOwnProperty("history") ? response["history"].trim() : "";

		   var has_voice_icons = false;
		   var displayed_entities = {};
		   if (context_suggestion != "") {
			   $("#context_entities").html("");
			   var context_tokens = context_suggestion.split(",");
			   for (var x = 0; x < context_tokens.length; x++) {
			     var token = context_tokens[x];
			     var entity_icon = token;
			     // ename_in_ulang : entity name in user language
			     var ename_in_ulang = "";
			     if (token.indexOf('(') >= 0) {
			       entity_icon = token.slice(0, token.indexOf('('));
			       ename_in_ulang =  token.slice(token.indexOf("(") + 1, -1);
			     }
			     if (displayed_entities[entity_icon] || !entity_icon) {
			       continue;
			     }
			     displayed_entities[entity_icon] = true;
			     entity_name2icon[ename_in_ulang] = entity_icon;
			     $("#context_entities").append(entityHtmlFromToken(entity_icon, ename_in_ulang));
			   }
		   }
		   if (general_suggestion != "") {
			   $("#general_entities").html("");
			   var tmp_html = "";
			   var per_cat_entities = general_suggestion.split("],[");
			   for (var i = 0; i < per_cat_entities.length; i++) {
				   if (per_cat_entities[i].indexOf("[") >= 0) {
				      per_cat_entities[i] = per_cat_entities[i].slice(per_cat_entities[i].indexOf("[") + 1);
				   } else if (per_cat_entities[i].indexOf("]") >= 0) {
				      per_cat_entities[i] = per_cat_entities[i].slice(0, per_cat_entities[i].indexOf("]"));
				   }
				   if (per_cat_entities[i] == "") {
				     continue;
				   }
				   // One category per row, each row is a <div>
				   tmp_html +=  "<div class='per_cat_entities'>";
				   var general_tokens = per_cat_entities[i].split(",");
				   for (var x = 0; x < general_tokens.length; x++) {
				     var token = general_tokens[x];
				     var entity_icon = token;
				     var ename_in_ulang = "";
				     if (token.indexOf('(') >= 0) {
				       entity_icon = token.slice(0, token.indexOf('('));
				       ename_in_ulang =  token.slice(token.indexOf("(") + 1, -1);
				     }
				     if (displayed_entities[entity_icon] || !entity_icon) {
				       continue;
				     }
				     displayed_entities[entity_icon] = true;
				     entity_name2icon[ename_in_ulang] = entity_icon;
				     tmp_html += entityHtmlFromToken(entity_icon, ename_in_ulang);
				   }
				   tmp_html += "</div>";
			   }
			   $("#general_entities").html(tmp_html);
		   }
		   if (history_suggestion != "") {
		     watch_history = history_suggestion.split(",");
		   }
			   
		   // Animation did not work, cause various problem, disabled
		   var animating = 0; 

		   // Handle entity icon click, play audio, issue a search
		  $(".entity_icon_button").on('click touchend', function() {
		    if (event.type == "click") documentClick = true;
		    if (!documentClick) {
		      return;
		    }
		    var entity_name = $(this).attr("title");
		    // A stupid hack, mobile device somehow trigger this event multiple times within a second, cause echo.
		    if (entity_name == last_clicked_entity) {
			 return;
		    }
		    last_clicked_entity = entity_name;
		    setTimeout(clearLastClickedEntity, 3000);
		    var icon = $(this).find("img").attr("src");
		    RequestEntitySuggestion(icon.slice(icon.indexOf("/") + 1), /*query*/
					    true, /*async*/
					    user_skills["id"],  /*getip */
					    "1",  /*context*/
					    "0",  /*general*/
					    "0"   /*history*/);
		    
		    var image_url = 'url("' + icon + '")';
		    $(".querybox").css("background-image", image_url);
		    $(".querybox").css("background-position", 'right');
		    $(".querybox").val(entity_name);
		    $(".tablinks").eq(1).trigger("click");

		    if (speaker) { 
			var audio_file = $(this).attr("audio");
			audio = document.createElement("audio");
			audio.src = audio_file;
			audio.play();
		    }
		  });

          }
	});
}

  // Basic search function
  function Search(query) {
      active_rightcol = -1;
      result_entities = [];
      var url = "https://www.googleapis.com/customsearch/v1?";
      var ytdata = "https://www.googleapis.com/youtube/v3/search?";
      if (active_vertical == video_vertical) {
             url = ytdata;
             url += "safeSearch=strict";
             url += "&part=snippet";
             url += "&type=video";
             url += "&maxResults=50";
      } else {
	     url += "&cx=" + $("#cx").val();
      }
      url += "&key=" + $("#key").val();
      original_query = query;
      // TODO: extend this with a query rewrite module
      query = convertBadWord(query);
      query = query.replace(/\s+/g, "%20");
      url += "&q=" + query;
      var lang = $(".lang_selection").val();
      if (lang == "en") {
        // Adjust ranking to append appropriate context keywords
        // TODO, make this more dynamic, based on user age and category
        if (context === undefined) {
          url += "%20for%20kids";
        } else if (context !== "") {
          url += "%20" + context.replace(/ /g, "%20");
        }
      } else {
        if (lang == "zh") {
          url += "&relevanceLanguage=zh-Hans";
        } else {
          url += ("&relevanceLanguage=" + lang);
        }
      }
      $("#general_entities").hide();
      $("#verticals").show();
      $("#content").html("");
      $("#content").show();
      var out = "";
  
      if (deb_level) {
        if (deb_level > 0) {
		out = "<b>" + url + "</b><br/>";
        } 
      }
      // Create a temporary resultwrap to measure screen width
      $('<div>', {'class' : 'resultwrap'}).appendTo("body");
      var result_width = parseInt($(".resultwrap").css("width").slice(0, -2));
      $(".resultwrap").remove();
      // Estimate how many results we can show in one row.
      var items_per_row = Math.floor(($(document).width()-100) / result_width);
      if (original_query == "" && watch_history.length > 0 && active_vertical == video_vertical) {
        var displayed_items = 0;
        for(var i = 0; i < watch_history.length; i++) {
	    // item contains "<ytid>;<title>;<displaylink>"
            var item = watch_history[i];
            var item_content = item.split(";");
            if (item_content.length < 3) {
              continue;
            }
            if (displayed_items % items_per_row == 0) {
	      out += '<table class="tablerow_history"><tr>';
            }
            var history_url = "https://www.youtube.com/watch?v=" + item_content[0];
            var thumbnail = "https://i.ytimg.com/vi/" + item_content[0] + "/mqdefault.jpg";
            out += "<td>";
            out += '<div class="resultwrap" title="' +  item_content[1] + '" displaylink = "' + item_content[2] + '"   url="' + history_url + '">';
            out += '<div class="resulttitle"> <p class="titletxt">' + item_content[1]  + '</p> </div>';
	    out +=  item_content[2];
            out += "<br/>";
            out += '<div class="imagewrap"';
            var click_url = "https://www.youtube.com/embed/" + item_content[0] + "?&autoplay=1&rel=0&fs=0&showinfo=0&autohide=1&modestbranding=1&enablejsapi=1"; 
            out += ">";
            out +=  '<div class="linkdata" linkdata="' + click_url + '" imagedata="' + thumbnail + '">';
            out += "<div class=\"play_button\"></div>";
            out += '<img class="result_thumbnail" src="' + thumbnail + '">';
            out += "</div>"; <!-- linkdata -->
            out += "</div>"; <!--imagewrap -->
            out += "</div>"; <!--resultwrap-->
            out += "</td>";
            displayed_items++;
            if (displayed_items % items_per_row == 0) {
		    out += "</tr></table>";
                    if (watch_history.length - 1 - i  < items_per_row) {
                       break;
                    }
            }
        }
        if (displayed_items % items_per_row != 0) {
          out += "</tr></table>";
        }
        $("#content").html(out);
      }
      out = "";
      $.get(url, function(response, status) {
        if (!response.items && $("#content").html() == "") {
          out +=  "<b>  Oops, nothing found here </b><br/><br/>";
	  $("#content").html(out);
          ui_status = 1;
          return;
        }
        var displayed_items = 0;
        var digest_arr = [];
        var result_urls = [];
        var nonyt_result_urls = [];

        for(var i = 0; i < response.items.length; i++) {
            var item = response.items[i];
            var result = {};
	    if (active_vertical == video_vertical) {
              result =  parseYoutubeSearchResponse(item);
            } else {
              result =  parseGoogleSearchResponse(item);
            }
	    if (result.hasOwnProperty("failed")) {
		    if (deb_level && deb_level > 1) {
		      out += "<br/>" + result["debug"];
		    }
		    continue;
            }

            var row = Math.round(displayed_items / items_per_row);
            if (displayed_items % items_per_row == 0) {
		    out += '<table class="tablerow" ' + row + '"><tr>';
            }
            out += "<td>";
            var title_no_quote = result["title"].replace(/[;,\"\']/g, " ");
            var dlink_no_quote = result["displayLink"].replace(/[;,\"\']/g, " ");
            out += '<div class="resultwrap" title="' +  title_no_quote + '" displaylink = "' + dlink_no_quote + '"   url="' + result["url"] + '">';
            var title = result["title"];
            out += '<div class="resulttitle"> <p class="titletxt">' + title + ' </p> </div>';
	    if (result.hasOwnProperty("displayLink")) {
		    out +=  result["displayLink"];
            }
            if (result.hasOwnProperty["price"] > 0) {
		    out +=  "&nbsp;Price: $" + result["price"];
            }
            if (result.hasOwnProperty["Duration"] > 0) {
		    out +=  "&nbsp;Duration: $" + result["duration"];
            }
            out += "<br/>";
            out += '<div class="imagewrap"';
            var click_url = result["url"];
            if (result.hasOwnProperty("ytid")) {
              click_url = "https://www.youtube.com/embed/" + result["ytid"] + "?&autoplay=1&rel=0&fs=0&showinfo=0&autohide=1&modestbranding=1&enablejsapi=1"; 
            } else if (result.hasOwnProperty("playlistid")) {
              click_url = "https://www.youtube.com/embed/videoseries?list=" + result["playlistid"];
            }
            out += ">";
            
            // Get clean text (remove punct) to retrieve related entities (per result and overall for the query)
            // Backend will do text match b/w clean_text and entity names to compute related entities.
            var title_clean_txt = result["title"].replace(/[.,\/#!$%\^&\*;:{}=\-_`~()|\"\']/g, " ");
	    var descript_clean_txt = "";
            if (result.hasOwnProperty("description")) {  
		 descript_clean_txt = result["description"].replace(/[.,\/#!$%\^&\*;:{}=\-_`~()|\"\']/g, " ");
            }
            digest_arr.push(title_clean_txt + "|" +  descript_clean_txt);
            out +=  '<div class="linkdata" title_clean_txt="' + title_clean_txt  +'"  linkdata="' + click_url + '" imagedata="' + result["thumbnail"] + '">';
            if (result.hasOwnProperty("ytid") || result.hasOwnProperty("playlistid")) {
              out += "<div class=\"play_button\"></div>";
            }
            out += '<img class="result_thumbnail" src="' + result["thumbnail"] + '">';
            out += "</div>"; <!-- linkdata -->
            out += "</div>"; <!--imagewrap -->
            out += "</div>"; <!--resultwrap-->
            out += "</td>";
            if (displayed_items % items_per_row == items_per_row - 1) {
		    out += "</tr></table>";
            }
            ++displayed_items;
            if (deb_level && deb_level > 1) {
		out += '<br/>' + result["debug"];
	    }
            if (!result.hasOwnProperty("ytid") && !result.hasOwnProperty("playlistid")) {
		    nonyt_result_urls.push(result["url"]);
            }
	    result_urls.push(result["url"]);
          }
          if (displayed_items % items_per_row != 0) {
		    out += "</tr></table>";
          }
          out += "<br/><br/>";
          $("#content").append(out);
          ui_status = 1;

          /*
                // Request related entity
	        var http = new XMLHttpRequest();	
		var url = ToggleDevPhp("fetch_eggs");
	        var logging_params = fillInLoggingParams();
		var params = "q=" +  query + "&ip=" + logging_params["ip"] + "&v=" + logging_params["vertical"] + "&lang=" + logging_params["lang"];
	        params += "&digests=" + digest_arr.join(",") + "&impressions=" + result_urls.join(",");
		http.open("POST", url, true);

		//Send the proper header information along with the request
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		http.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
			if (this.responseText.length > 0) {
			   var entity_groups = this.responseText.split(",");
                           if (entity_groups.length > 0) { 
			     // 1st up to 2nd last are for per-result suggestion
			     // Not being used for now
                             for (var i = 0; i < entity_groups.length - 1; i++) {
			       result_entities.push(entity_groups[i]);
                             }
			     // last group is for global suggestion
			     setEntitySuggesions(entity_groups[entity_groups.length - 1]);
                           }
                        }
		    }
		}
		http.send(params);
         */

          // Handle result click, show iframe window
          $("[linkdata]").click(function(){
	      skillUpdate(["resultclk"], 3);
              var url = $(this).attr('linkdata');
              if (url === "") {
                return;
              }
              var hostname = url_domain(url);
              var title_clean_txt = $(this).attr('title_clean_txt');
	      var iframe_topbar = '<div class="iframe_topbar"><button class="closeIframe" id="closeIframe" onclick="CloseIframe()"></button></div>';
	      var iframe = '<iframe class="iframe-window"  src="' + url + '" ></iframe>';
	      $("#context_entities").hide();
	      $("#general_entities").hide();
              $("#verticals").hide();
	      // $("#closeContent").hide();
              $("#content").hide();
	      $("#iframe").html(iframe_topbar + iframe);
               
               var new_qentities_styles = { 'position':'fixed', 'top':'1px', 'left':'3px', 'align-self':'left', '-webkit-align-self':'left'};
               var iframe_topbar_height_style = $(".iframe_topbar").css("height");
               var newq_height = Math.floor(parseInt(iframe_topbar_height_style.slice(0, -2)) * 0.75);
               var querybox_pic_height = newq_height * 0.9;
               new_qentities_styles["height"] = newq_height.toString() + 'px';
               $("h2").hide();
               $('#q_entities').css(new_qentities_styles);
               $('.oPenButton').css({  'height': newq_height.toString() + 'px'});
	       querybox_style_backup = $(".querybox").cssAll('background-size');
               $('.querybox').css({  "background-size": querybox_pic_height.toString() + 'px ' +  querybox_pic_height.toString() + 'px'});
               $('#oPenButtonImg').css({  'height': newq_height.toString() + 'px'});
              
              ui_status = 2;
          }); 
          
	  $(".resultwrap").on('click', function() {
	      var logging_params = fillInLoggingParams();
	      // c means "clicks"
              // qtype probably should rename to "event_type"
	      logging_params["qtype"] = 'c';
	      logging_params["title"] = $(this).attr("title");
	      logging_params["displaylink"] = $(this).attr("displaylink");
              logging_params["click_url"] = $(this).attr("url");  
              var myidx = $(this).index(".resultwrap");
              logging_params["click_pos"] = myidx;  
	      var php_file = ToggleDevPhp("logging");
	      $.post(php_file, logging_params);
          });
      });
    }

var deft_query = "DIY OR lego set OR mad science OR nursery rhymes OR cartoon OR ESL OR spelling bee OR making cake OR barbie OR math OR health";

$(document).ready(function() {
        // This should absolutely be the first line of code!!!
        getUrlParameter();
        var getip = "1";
        if (baseURI.indexOf("cse.html") >= 0) {
          getip = "developer";
        }
        RequestEntitySuggestion("" /*query*/,
                                false /*async*/,
                                getip,
                                1 /*context*/,
                                0 /*general*/,
                                1 /*history*/);
        qentities_style_backup = $("#q_entities").cssAll('top,left,width,height');
        general_entities_style_backup = $("#general_entities").cssAll('position,top,left,bottom,border-style,border,margin,padding,overflow');
        h2_display_backup = $("h2").css('display');
        $("#cx").val(cse_cx[active_vertical]);
        // cgi_q is for debuging or scraping for analysis
        if (cgi_q) {
          deft_query = cgi_q;
	      var logging_params = fillInLoggingParams();
	      // s means "scraping"
	      logging_params["qtype"] = 's';
	      logging_params["query"] = cgi_q;
	      var php_file = ToggleDevPhp("logging");
	      $.post(php_file, logging_params);
          Search(deft_query);
        } else {
	      var logging_params = fillInLoggingParams();
	      // "h" means "home page visit"
	      logging_params["qtype"] = 'h';
	      var php_file = ToggleDevPhp("logging");
	      $.post(php_file, logging_params);
       }

        fillingBadWordsDict();
        if (active_vertical != 0) {
             Search("");
        }

        // Request IP address from backend, it is a synchronized call, causing latency,
        // It is necessary to be synchonrized call because RequestEntitySuggestion below needs IP address
        // Another possibility is to merge getip and RequestEntitySuggestion into one call.
        // Move to cookie or user registration would reduce this latency
        /*
	var php_file = ToggleDevPhp("getip");
        $.ajax({
	  url: php_file,
	  dataType: 'json',
	  async: false,
	  data: '',
	  success: function(data) {
		  for (key in data) {
		     if (key == "id") {
		       user_skills[key] = data[key];
		     } else {
		       user_skills[key] = parseInt(data[key]);
		     }
		  }
	  }
	});*/

    var vertical_icons = ["videoicon", "appicon", "bookicon"];
   
    // Switch tabs 
    $(".tablinks").click(function(){
      var myidx = $(this).index(".tablinks");
      var icon = vertical_icons[myidx];
      $(this).parents("ul").find("li").eq(active_vertical).attr("class", "");
      $(this).parent().attr("class", "active");
      active_vertical = myidx; 
      if (myidx == 0) {
 	 RequestEntitySuggestion("", /*query*/
                                 true,
                                 user_skills["id"],
                                 "0", /*context_entities*/
                                 "1", /*general_entities*/
                                 "0"  /*history*/);
          CloseContent();
      } else {
		$("#cx").val(cse_cx[active_vertical]);
		$(".oPenButton").trigger("click");
      }
    });

    // Click anywhere on the top bar to close iframe
    $(".iframe_topbar").on('click touch', function(){
	CloseIframe();
	return;
    });

    // Handle oPen box click to start a search
    $(".oPenButton").on('click touch', function(){
      if (active_vertical == 0) {
        active_vertical = 1;
      }
      if (ui_status == 2) {
        CloseIframe();
        return;
      }
      var logging_params = fillInLoggingParams();
      // b means "query box"
      logging_params["qtype"] = 'b';
      var php_file = ToggleDevPhp("logging");
      $.post(php_file, logging_params);
      try {
	      Search(logging_params["query"]);
      }
      catch (err) {
          var out =  "<b>  Oops, nothing found here </b><br/><br/>";
	  $("#general_entities").hide();
          $("#content").html(out);
          $("#verticals").show();
	  // $("#closeContent").show();
          $("#content").show();
          ui_status = 1;
      }
    });
	$(".querybox").on('focus', function(e) {
          e.preventDefault();
	  $(this).css("background-image", "none");
          $(this).val("");
        });

	$(".querybox").on('keyup change cut', function(e) {
            var event = e || window.event;
            var start_search = false;
            var delete_pressed = false;
	    if (  event.type == "keyup" ||  event.type == "cut") {
	        var charCode = event.which || event.keyCode;
	        if (charCode == 13 ) {
		    start_search = true;
	        } else if (charCode == 8 || charCode == 127 || event.type == "cut") {
		    delete_pressed = true;
                }
            }


	    if ( start_search) {
	      $(this).blur();
	      var logging_params = fillInLoggingParams();
	      // b means "query box"
	      logging_params["qtype"] = 'b';
	      var php_file = ToggleDevPhp("logging");
	      $.post(php_file, logging_params);
	      try {
		      Search(logging_params["query"]);
	      }
	      catch (err) {
		  var out =  "<b>  Oops, nothing found here </b><br/><br/>";
		  $("#general_entities").hide();
		  $("#content").html(out);
		  $("#verticals").show();
		  $("#content").show();
		  ui_status = 1;
	      }
	    } else if (!delete_pressed) {
	      var query = userQuery();
              if (!query) {
                return;
              }
              if (entity_name2icon.hasOwnProperty(query)) {
		 var image = 'url("entity_icons/' + entity_name2icon[query] + '")';
		 $(this).css("background-image", image);
		 $(this).css("background-position", 'right');
		 $(".oPenButton").trigger("click");
              } else {
                 RequestEntitySuggestion(query,
                                         true, /*async*/
                                         user_skills["id"],
                                         "1", /*context*/
                                         "0", /*general*/
                                         "0"  /*history*/);
              }
            } else {
	      var query = userQuery();
              if (!entity_name2icon.hasOwnProperty(query)) {
		 $(this).css("background-image", "none");
              }
            }
	});

        // Whenever user switch language,we should do a Request entity suggestion for two reasons
        // 1) Some entities does not have translation in user pick language they should not be shown
        // 2) Retrieve entity name in user language for search.
	$(".lang_selection").change(function() {
	  RequestEntitySuggestion("",
	  			  true, /*async*/
				  user_skills["id"],
				  "1", /*context*/
				  "1", /*general*/
				  "0"  /*history*/);
	});
});
