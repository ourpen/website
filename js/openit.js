var myip = "";
var deb_level;
var devphp_set = {};
var context; 
var current_page = 0;
var cgi_q = "";
var documentClick = false;
var watch_history = [];
var referrer =  document.referrer;
var uri = document.documentURI ? document.documentURI : document.URL;
var digest = {};
var isIOS = ((/iphone|ipad/gi).test(navigator.appVersion));
var ui_status = 1; // 0: entity selection; 1: search result page; 2: challenge page 3:watchpage
var deft_query = "DIY OR lego set OR mad science OR nursery rhymes OR cartoon OR ESL OR spelling bee OR making cake OR barbie OR math OR health";
var srp_results_per_row = 1;
var icons_per_row = 1;
var cn_ver = 0;
var player_width = 0;
var player_height = 0;
var adwords_terms = {'drseuss' : 'dr seuss',
                       'flyguy' : 'fly guy',
                       'brainpop' : 'brain pop',
                       'brainpopjr' : 'brain pop jr',
                       'afterschool' : 'after school'};
var prestore_entities = {'dr seuss' : 'dr_seuss.png',
                           'fly guy' : 'fly_guy.png',
                           'brain pop' : 'brain_pop.png',
                           'brain pop jr' : 'brain_pop_jr.png'};
 

// According to some stackflow page, this can prevent unintentional touchmove become a click event
$(document).on('touchstart', function() {
    documentClick = true;
});
$(document).on('touchmove', function() {
    documentClick = false;
});

function isDebugIp(ip) {
  return ip.indexOf("deb.") >= 0;
}

function peerOffDebIpPrefix(ip) {
  var new_ip = ip;
  if (isDebugIp(ip)) {
    new_ip = ip.slice(ip.indexOf("deb.") + 4);
  }
  return new_ip;
}
   
// A util to retrieve and set a list off css property
(function($)
{
    $.fn.cssAll = function(css)
    {
        var obj = {};

        if(this.length)
        {
            var css = css.split(',');
            var params = [];

            for(var i=0,ii=css.length; i<ii; i++)
            {
                params = css[i].split(':');

                obj[$.trim(params[0])] = $(this).css($.trim(params[1] || params[0]));
            }
        }

        return obj;
    };
})(jQuery);

function embedYTLink(ytid) {
  return "https://www.youtube.com/embed/" + ytid + "?&autoplay=1&rel=0&fs=0&showinfo=1&autohide=1&modestbranding=1&enablejsapi=1";
}

function watchYTLink(ytid) {
  return "https://www.youtube.com/watch?v=" + ytid;
}

function thumbnailYTLink(ytid) {
  return "https://i.ytimg.com/vi/" + ytid + "/mqdefault.jpg"
}


function queryEntityImgHtml(img_file) {
    return '<img class="queryEntityImg" src="' + img_file + '">';
}	     

function queryEntityTextHtml(txt) {
    return '<b>' + '&nbsp;' + txt + '</b>';
}	     
 
function htmlFromUserQueryTerm(query, icon) {
  if (icon != "") {
	    return queryEntityImgHtml("entity_icons/" + icon);
  }
  return queryEntityTextHtml(query);
}
 
function composeWatchpageTopBar() {
  var html = ""
  html += '<div class="watchpage_navigation">';
  html += '<button class="back_to_kg">';
  html += '<img class="vertical_img" src="icons/open/knowledge_entity.jpg"/>';
  html += '</button>';
  html += '<button class="back_to_hp">';
  html += '<img class="vertical_img" src="icons/open/red_magnifier.png"/>';
  html += '</button>';
  html += '</div> <!-- watchpage_navigation -->';
  html += '<div class="andquery_entities">';
  var main_query = $("#q").val().toLowerCase();
  var main_query_icon = entity_name2icon.hasOwnProperty(main_query) ? entity_name2icon[main_query] : ""; 
  var refine_query = "";
  var refine_query_icon = "";
  if ($("#refine_q").val()) {
    refine_query = $("#refine_q").val().toLowerCase();
    refine_query_icon = entity_name2icon.hasOwnProperty(refine_query) ? entity_name2icon[refine_query] : "";
  }
  if (main_query != "") {
    html += htmlFromUserQueryTerm(main_query, main_query_icon);
  }  
  if (refine_query != "") {
    html += htmlFromUserQueryTerm(refine_query, refine_query_icon);
  }  
  html += '</div><!-- andquery_entities -->';
  return html;
}

function composeRefinementBar() {
   var html = '<div class="refinementbar">';
   html += '<div class="related_search"> <input type="text" class="refinement_query" name="refine_q" id="refine_q" size="10"  value=""/></div>';
   html += '<div class="refinements">'; 
   var cnt = 0;
   var entity_width = parseInt($(".entity_icon_button").css("width").slice(0, -2));
   var entities_per_row = 0.8 * Math.floor($(document).width()) / entity_width;
   for(var i=0; i < result_entities.length; i++) {
     // entity_string format "<icon_file>(<entity_name>)"
     var entity = result_entities[i];
     var icon = iconFromUserQueryEntity(entity);
     var name = entityNameFromUserQueryEntity(entity);
     var main_query = $("#q").val().toLowerCase();
     var main_query_icon = entity_name2icon.hasOwnProperty(main_query) ? entity_name2icon[main_query] : ""; 
     if (icon == "" || name == "" || icon == main_query_icon) {
       continue;
     }
     html += entityHtmlFromToken(icon, name);
     if (++cnt >= entities_per_row) {
       break;
     }
   }
   for(var i=0; i < Math.min(entities_per_row - cnt, context_entities.length); i++) {
     var entity_icon = context_entities[i];
     var main_query = $("#q").val().toLowerCase();
     var main_query_icon = entity_name2icon.hasOwnProperty(main_query) ? entity_name2icon[main_query] : ""; 
     if (icon == "" || name == "" || entity_icon == main_query_icon) {
       continue;
     }
     html += entityHtmlFromToken(entity_icon, entity_icon2name[entity_icon]);
     if (++cnt >= entities_per_row) {
       break;
     }
   }
   html += '</div> <!--refinements-->';
   html += '</div> <!--refinementbar-->';
   return html;
}

// Input: result with title, displaylink, ytid, thumbnail
function resultHtml(result, resulttype) {
     if (!result.hasOwnProperty("title") || result["title"] == "" ||
         !result.hasOwnProperty("displaylink") || result["displaylink"] == "" ||
         !result.hasOwnProperty("ytid") || result["ytid"] == "" ||
         !result.hasOwnProperty("thumbnail") || result["thumbnail"] == "") {
        return "";
     }
     var local_cache = (cn_ver == 1);
     var title_no_quote = result["title"].replace(/[;,\"\']/g, " ");
     var dlink_no_quote = result["displaylink"].replace(/[;,\"\']/g, " ");
     var html = '<div class="resultwrap" local_cache="' + local_cache.toString()  + '" rtype="' + resulttype + '"  title="' +  title_no_quote + '" displaylink = "' + dlink_no_quote + '"   ytid="' + result["ytid"] + '">';
     html += '<div class="resulttitle"> <p class="titletxt">' + result["title"] + ' </p> </div>';
     html += '<div class="displaylink">' + result["displaylink"] + '</div>';
     html += '<div class="imagewrap">';
     html += "<div class=\"playbutton\"></div>";
     html += '<img class="result_thumbnail" src="' + result["thumbnail"] + '">';
     html += "</div> <!--imagewrap -->";
     html += "</div> <!--resulttype-->";

     return html;
}


function parseLocalDBSearchResponse(item) {
  return parseYoutubeSearchResponse(item);
}
 
// Parse YouTube search data API, response structure is different than google CSE API
function parseYoutubeSearchResponse(item) {
  var result = {};
  var title = item.snippet.title;
  if (title.length > 43) {
	  title = title.slice(0, 40) + "...";
  }
  result["title"] = title;
  result["thumbnail"] = item.snippet.thumbnails.medium.url;
  result["description"] =  item.snippet.description;
  result["ytid"] = item.id.videoId;
  result["channel"] = item.snippet.channelId;
  var channelTitle = item.snippet.channelTitle;
  if (channelTitle.length > 24) {
    channelTitle = channelTitle.slice(0, 21) + "...";
  }
  result["displaylink"] = channelTitle;
  if (deb_level && deb_level > 1) {
    result["debug"] = JSON.stringify(item);
  }
  if (!result.hasOwnProperty("ytid") || !result.hasOwnProperty("title") || !result.hasOwnProperty("thumbnail") || !result.hasOwnProperty("displaylink")) {
    result["failed"] = true;
    if (deb_level && deb_level > 1) {
	result["debug"] += '<br/><b>Something is missing!</b><br/>';
    }
  }
  return result;
}

// See google CSE config here: https://cse.google.com/cse/setup/basic?cx=014484386212996657006%3A-hqy9dxsm08
var api_key = "AIzaSyBt_9oYNnRByBH-6jd4O3a2-vkG-OHsJYk";  // video
var cse_cx = "014484386212996657006:-hqy9dxsm08";  // video
// Initials for each vertical, "k": knowledge entities. v:video, w:watch page, c:challenge
var vertical_name = ["kg", "vid",  "cha", "wp"];
var video_vertical = 1;
// Common prefix in case we want to debug a new CSE with different suffix
var cse_prefix = "014484386212996657006:";

var curse_words  = ["bull shit", "ass hole", "fuck", "fucks", "fucked", "fucking", "fucker", "anus",  "bitch", "asshole", "sucker", "damn", "fuckass", "motherfucker", "pussy", "shit", "idiot", "nigro", "niggers", "butt", "jackass", "testicle", "whore"];
var sex_seeking = ["xxx", "porn", "nake", "nak girl", "nud", "nak", "naked", "nude", "nudity", "blow job", "sex", "dick", "butt", "genital", "make love", "intercourse", "cock", "handjob", "humping", "penis", "vagina"];
var lgbt_words = ["gay", "transgender", "transgender", "homosexual", "lesbian", "queer", "asexual"];
var horror_phrases = ["ghost", "torture", "devil", "demon", "hell", "zombie", "bloodlust", "abhorring", "brutal", "choking", "corpse", "decapitated", "suicide", "vampire", "mutilation",  "horror", "horror movies", "horror movie", "scary movies", "scary movie", "murdur"];
var broad_queries = ["movie", "movies", "apps", "app", "book", "books", "game", "games", "crafting", "art"];

var dict_initialized = false;
var curse_dict = {};
var sexseeking_dict = {};
var lgbt_dict = {};
var horror_dict = {};
var broadphrase_dict = {};
var entity_name2root = {};
var entity_name2icon = {};
var entity_icon2name = {};
var root2ctxt = {"technology" : "for kids", "science" : "for kids", "health" : "for kids", "social studies" : "for kids"};
var context_entities = [];
var result_entities = [];
var cur_context_class = "#context_entities";
var cur_srp_class = ".srp_content";
var cur_querybox = ".querybox";

// Prevent multiple touch/click causing echo. If we get click event for same entity within 3 sec, ignore later ones.
var last_clicked_entity = "";
function clearLastClickedEntity() {
  last_clicked_entity = "";
}

function contextSwitchTo(ui) {
  // ui:0 knowledge graph UI
  // ui:1 video search UI
  // ui:2 challenge UI, not implemented yet
  // ui:3 watch page UI
  ui_status = ui;
  if (ui == 3) {
      cur_querybox = ".refinement_query";
      cur_context_class = ".refinements";
      cur_srp_class = ".relatedbar";
      $(".fullwidth").hide();
      $("#watchpage").show();
  } else {
      $(".iframe").html("");
      cur_querybox = ".querybox";
      cur_context_class = "#context_entities";
      cur_srp_class = ".srp_content";
  }
  if (ui < 2) { 
        $("#watchpage").hide();
        $(".fullwidth").show();
        $("ul").find("li").attr("class", "");
        $("ul").find("li").eq(ui).attr("class", "active");
        RequestEntitySuggestion(ui == 0 ? "openit.png" : "", /*query*/
			    true, /*async*/
			    peerOffDebIpPrefix(myip), /*getip*/
			    "1",  /*context*/
			    "0",  /*general*/
			    "0"   /*history*/);
        if (ui == 0) {
              $("#content").hide();
	      $("#general_entities").show();
        } else {
	      $("#general_entities").hide();
              $("#content").show();
        }
  }
}

function curUIStatus() {
  return ui_status;
}

function curUI() {
  return vertical_name[ui_status];
}

function curContextEntityClass() {
  return cur_context_class;
}

function curSrpClass() {
  return cur_srp_class;
}

function curQueryBoxClass() {
  return cur_querybox;
}

var general_entities_style_backup;

function fillingBadWordsDict() {
        if (dict_initialized) {
          return;
        }
	for (var x = 0; x < curse_words.length; x++) {    
	    curse_dict[curse_words[x]] = true;
	}
	for (var x = 0; x < sex_seeking.length; x++) {    
	    sexseeking_dict[sex_seeking[x]] = true;
	}
	for (var x = 0; x < lgbt_words.length; x++) {    
	    lgbt_dict[lgbt_words[x]] = true;
	}
	for (var x = 0; x < horror_phrases.length; x++) {    
	    horror_dict[horror_phrases[x]] = true;
	}
	for (var x = 0; x < broad_queries.length; x++) {    
	    broadphrase_dict[broad_queries[x]] = true;
	}
        dict_initialized = true;
}

// -1: nothing wrong 0: empty query, 1: too broad 2: bad manner 3: sex related 4: horror 
function checkQueryPhrase(query) {
  var terms = query.split(" ");
  if (terms == "") {
    return 0;
  }
  if (terms.length == 1 && broadphrase_dict[query]) {
    return 1;
  }
  for (var x = 0; x < terms.length; x++) {    
          var idx = x;
          var term = terms[x];
          var bigram = "";
          if (idx < terms.length - 1) {
            bigram = term + " " + terms[(idx + 1).toString()];
          }
	  if (curse_dict[term] || curse_dict[bigram]) {
	    return 2;
	  } 
	  if (sexseeking_dict[term] || sexseeking_dict[bigram]) {
	    return 3;
	  } 
	  if (lgbt_dict[term] || lgbt_dict[bigram]) {
	    return 3;
	  }
	  if (horror_dict[term] || horror_dict[bigram]) {
	    return 4;
	  }
  }

  return -1;
}

function convertBadWord(query) {
  query = query.replace(/\s+/g,' ');
  var query_class = checkQueryPhrase(query);
  switch (query_class) {
    case -1:
        return query;
    case 0:
        return deft_query;
    case 1:
	return query + " for kids";
    case 2:
        return "teaching children manners";
    case 3:
        return "gender education for kids";
    case 4:
        return "holloween kids";
  }
}

//user query entity in format of <icon>(entity_name)
// notice icon can be empty if user gives us a query string which do not match any entity in DB
function entityNameFromUserQueryEntity(entity) {
      var name_start = entity.indexOf("(");
      var name_end = entity.indexOf(")");
      if (name_start < 0 || name_end < 0) {
        return "";
      }
      return entity.slice(name_start + 1, name_end);
}
function iconFromUserQueryEntity(entity) {
      var name_start = entity.indexOf("(");
      var name_end = entity.indexOf(")");
      if (name_start < 0 || name_end < 0) {
        return "";
      }
      return entity.slice(0, name_start);
}

function composeUserQuery() {
    var user_query = "";
    var main_query = $("#q").val().toLowerCase().trim();
    var ctxt = "";
    var lang = $(".lang_selection").val();
    if (entity_name2root.hasOwnProperty(main_query)) {
        if (lang == "en") {
          if (root2ctxt.hasOwnProperty(entity_name2root[main_query]) || root2ctxt.hasOwnProperty(main_query)) {
            ctxt = "for kids";
          }
        }
    }
    user_query =  main_query;
    if (curUIStatus() == 3) {
      if (cn_ver > 0) {
        user_query = "";
      }
      var refine_query = $("#refine_q").val().toLowerCase().trim();
      if (refine_query != "") {
        if (user_query != "") {
          user_query +=  " AND ";
        }
        user_query +=  refine_query;
      }
    }
    if (ctxt != "") {
      user_query += " " + ctxt;
    }
    return user_query;
}

function fillInLoggingParams() {
  var logging_params = {
    "ip" : myip,
    "query" : composeUserQuery(),
    "referrer" :  referrer,
    "lang" :  $(".lang_selection").val(),
    "vertical" : curUI(),
  }
  return logging_params;
}

// In CGI, developer can put "?dev=<php_file>,<...>" to switch to a development version of php code.
function ToggleDevPhp(php_file) {
      if (devphp_set[php_file]) {
        php_file += ('-' + devphp_set[php_file]);
      }
      php_file += ".php";
      return php_file;
}

// Parse CGI parameters "?key=value&..."
function getUrlParameter() {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    devphp_set = {};
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === "deb") {
            deb_level = sParameterName[1] === undefined ? 0 : sParameterName[1];
        } else if (sParameterName[0] === "ip" && sParameterName[1] !== undefined) {
            myip = "deb." + sParameterName[1];
        } else if (sParameterName[0].indexOf("dev") == 0) {
            var dev_php = sParameterName[1] === undefined ? "" : sParameterName[1];
	    var phps = dev_php.split(",");
	    for (var x = 0; x < phps.length; x++) {    
		    devphp_set[phps[x]] = sParameterName[0];
            }
        } else if (sParameterName[0] === "context") {
            context = sParameterName[1] === undefined ? "" : sParameterName[1];
        } else if (sParameterName[0] === "q") {
            cgi_q = sParameterName[1] === undefined ? "" : sParameterName[1];
        } else if (sParameterName[0] === "api") {
            if (sParameterName[1] !== undefined) {
		    api_key = sParameterName[1];
	    }
        } else if (sParameterName[0] === "cse") {
            if (sParameterName[1] !== undefined) {
               if (sParameterName[1].indexOf(":") > 0) {
		    cse_cx = sParameterName[1];
               } else {
		    cse_cx = cse_prefix + sParameterName[1];
               }
            }
        }
    }
}

// Compute an icon html based on icon filename and entity name. Given entity name may be in non-En, it does not have a 1:1 match against icon filename
function entityHtmlFromToken(icon, name) {
     var dot_pos = icon.indexOf(".");
     var prefix = icon.substring(0, dot_pos); 
     has_voice_icons = true;
     var image =  "entity_icons/" + icon;
     var lang = $(".lang_selection").val();
     var audio_file  = "entity_mp3/" + lang + "/" + prefix + "_" + lang + ".mp3";
     var suggestion = '<button class="entity_icon_button" audio="' + audio_file + '" title="' + name + '">';
     suggestion += '<img class="entity_icon_img" src="' + image + '"><p class="entity_icon_label">' + name + '</p>';
     suggestion += '</button>';
     return suggestion;
}

function handleEntityRequestResponse(response) {
   if (response.hasOwnProperty("ip") && myip.indexOf("deb") < 0) {
	    myip = response["ip"];
   }
   var context_suggestion = response.hasOwnProperty("context") ? response["context"].trim() : "";
   var general_suggestion = response.hasOwnProperty("general") ? response["general"].trim() : "";
   var watch_history_suggestion = response.hasOwnProperty("watch_history") ? response["watch_history"].trim() : "";

   if (context_suggestion != "") {
	   context_entities = [];
	   $(curContextEntityClass()).html("");
           if (context_suggestion != "no_results") {
		   var context_tokens = context_suggestion.split(",");
		   for (var x = 0; x < context_tokens.length; x++) {
		     var token = context_tokens[x];
		     var entity_icon = token;
		     // ename_in_ulang : entity name in user language
		     var ename_in_ulang = "";
		     if (token.indexOf('(') >= 0) {
		       entity_icon = token.slice(0, token.indexOf('('));
		       ename_in_ulang =  token.slice(token.indexOf("(") + 1, -1);
		       if (ename_in_ulang.indexOf("|") >= 0) {
			 var root_cat = "";
		         var tmp = ename_in_ulang;
                         ename_in_ulang = tmp.slice(0, tmp.indexOf("|"));
		         root_cat = tmp.slice(tmp.indexOf("|") + 1);
		         entity_name2root[ename_in_ulang] = root_cat;
                       }
		     }
		     if (!entity_icon || context_entities.hasOwnProperty(entity_icon)) {
		       continue;
		     }
		     entity_name2icon[ename_in_ulang] = entity_icon;
		     entity_icon2name[entity_icon] = ename_in_ulang;
		     context_entities.push(entity_icon);
		     $(curContextEntityClass()).append(entityHtmlFromToken(entity_icon, ename_in_ulang));
		   }
           }
   }
   if (general_suggestion != "") {
	   $("#general_entities").html("");
	   var tmp_html = "";
	   var per_cat_entities = general_suggestion.split("],[");
	   for (var i = 0; i < per_cat_entities.length; i++) {
		   if (per_cat_entities[i].indexOf("[") >= 0) {
		      per_cat_entities[i] = per_cat_entities[i].slice(per_cat_entities[i].indexOf("[") + 1);
		   } else if (per_cat_entities[i].indexOf("]") >= 0) {
		      per_cat_entities[i] = per_cat_entities[i].slice(0, per_cat_entities[i].indexOf("]"));
		   }
		   if (per_cat_entities[i] == "") {
		     continue;
		   }
		   // One category per row, each row is a <div>
		   tmp_html +=  "<div class='per_cat_entities'>";
		   var general_tokens = per_cat_entities[i].split(",");
		   for (var x = 0; x < general_tokens.length; x++) {
		     var token = general_tokens[x];
		     var entity_icon = token;
		     var ename_in_ulang = "";
		     if (token.indexOf('(') >= 0) {
		       entity_icon = token.slice(0, token.indexOf('('));
		       ename_in_ulang =  token.slice(token.indexOf("(") + 1, -1);
		       if (ename_in_ulang.indexOf("|") >= 0) {
		         var root_cat = "";
		         var tmp = ename_in_ulang;
                         ename_in_ulang = tmp.slice(0, tmp.indexOf("|"));
		         root_cat = tmp.slice(tmp.indexOf("|") + 1);
		         entity_name2root[ename_in_ulang] = root_cat;
                       }
		     }
		     if (!entity_icon) {
		       continue;
		     }
		     entity_name2icon[ename_in_ulang] = entity_icon;
		     entity_icon2name[entity_icon] = ename_in_ulang;
		     tmp_html += entityHtmlFromToken(entity_icon, ename_in_ulang);
		   }
		   tmp_html += "</div>";
	   }
	   $("#general_entities").html(tmp_html);
   }
   if (watch_history_suggestion != "") {
     watch_history = watch_history_suggestion.split(",");
   }

   // Handle entity icon click, play audio, issue a search
   $("body").on('click', '.entity_icon_button', function() {
    if (event.type == "click") documentClick = true;
    if (!documentClick) {
      return;
    }
    var entity_name = $(this).attr("title");
    // A stupid hack, mobile device somehow trigger this event multiple times within a second, cause echo.
    if (entity_name == last_clicked_entity) {
	 return;
    }
    last_clicked_entity = entity_name;
    setTimeout(clearLastClickedEntity, 3000);
    var icon = $(this).find("img").attr("src");
    icon = icon.slice(icon.indexOf("/") + 1);
  
    logEntityClick(icon); 
    if (curUI() == "wp") {
	    $("#refine_q").val(entity_name);
	    $(".watchpage_topbar").html(composeWatchpageTopBar());
    } else {
       $("#q").val(entity_name);
       RequestEntitySuggestion(icon, /*query*/
			    true, /*async*/
			    peerOffDebIpPrefix(myip), /*getip*/
			    "1",  /*context*/
			    "0",  /*general*/
			    "0"   /*history*/);
       if (curUI() != "vid") {
          contextSwitchTo(1);
       }
       var audio_file = $(this).attr("audio");
       audio = document.createElement("audio");
       audio.src = audio_file;
       audio.play();
    } 
    
    var image_url = 'url("entity_icons/' + icon + '")';
    $(curQueryBoxClass()).css("background-image", image_url);
    $(curQueryBoxClass()).css("background-position", 'right');
    $(curQueryBoxClass()).val(entity_name);
    $(curSrpClass()).html("");
    $(".watch_history").hide();
    logSearch();
    Search(composeUserQuery(), 50);
  });
}

// Ask backend to provide entity based on  the latest query (either the letter typed so far, or the icon just clicked)
// Context: 1/0,  if 1, ask for context entities based on context ( the latest query, either the letter typed so far, or the icon just clicked)
// General: 1/0, if 1, ask entities based on user interest (ranked top)
function RequestEntitySuggestion(query, asynchronize, getip, context, general, history) {
	var php_file = ToggleDevPhp("entity_suggestion");
        var request = {};
        request["getip"] = getip;
        request["q"] = query;
        request["lang"] = $(".lang_selection").val();
        request["c"] = context;
        request["g"] = general;
        request["h"] = history;
        $.ajax({
	  url: php_file,
	  dataType: 'json',
	  async: asynchronize,
	  data: request,
	  success: handleEntityRequestResponse
	});
}

function YTSearchUrl(query, ctxt, lang, num_results) {
      query = query.replace(/\s+/g, "%20");
      var url = "https://www.googleapis.com/youtube/v3/search?";
      url += "safeSearch=strict";
      url += "&part=snippet";
      url += "&type=video";
      url += "&maxResults=" + num_results.toString();
      url += "&key=" + api_key;
      url += "&q=" + query;
      if (lang == "en") {
	      if (ctxt !== "") {
		  url += "%20" + ctxt.replace(/ /g, "%20");
	      }
      }
      if (lang == "zh") {
          url += "&relevanceLanguage=zh-Hans";
      } else {
          url += ("&relevanceLanguage=" + lang);
      }
      return url;
}

function resultsPerRow(width) { 
      // Create a temporary resultwrap to measure screen width
      $('<div>', {'class' : 'resultwrap'}).appendTo("body");
      var result_width = parseInt($(".resultwrap").css("width").slice(0, -2));
      $(".resultwrap").remove();
      $('<div>', {'class' : 'entity_icon_button'}).appendTo("body");
      var icon_width = parseInt($(".entity_icon_button").css("width").slice(0, -2));
      $(".entity_icon_button").remove();
      // Estimate how many results we can show in one row.
      return [Math.floor(width / result_width), Math.floor(width / icon_width)];
}

function setPlayerSize() {
      // Create a temporary resultwrap to measure screen width
      $('<div>', {'class' : 'iframe'}).appendTo("body");
      player_width = parseInt($(".iframe").css("width").slice(0, -2));
      player_height = parseInt($(".iframe").css("height").slice(0, -2));
      $(".iframe").remove();
}

function watchHistoryHtml(results_per_row) {
        if (watch_history.length < results_per_row) {
          return "";
        }
        var out = "";
        var displayed_items = 0;
        for(var i = 0; i < watch_history.length; i++) {
	    // item contains "<ytid>;<title>;<displaylink>"
            var item = watch_history[i];
            var item_content = item.split(";");
            if (item_content.length < 3) {
              continue;
            }
            var ytid = item_content[0];
            var thumbnail = thumbnailYTLink(ytid);
            var result = new Array();
            result["ytid"] = item_content[0];
            result["title"] = item_content[1];
            result["displaylink"] = item_content[2];
            result["thumbnail"] = thumbnail;
            out += resultHtml(result, "srpresult");
            displayed_items++;
            if (displayed_items % results_per_row == 0) {
                    // Display at most two rows of watch history videos
                    if (watch_history.length - 1 - i  < results_per_row) {
                       break;
                    }
            }
        }
        return out;
}

function digestLocalDBSearchResponse(response) {
        var out = "";
        var digest_arr = [];
        var result_ytids = [];
        digest = {};
        if (!response.items) {
          return;
        }
        var displayed_items = 0;
        for(var i = 0; i < response.items.length; i++) {
            var item = response.items[i];
            var result = parseLocalDBSearchResponse(item);
            
	    if (result.hasOwnProperty("failed")) {
		    if (deb_level && deb_level > 1) {
		      out += "<br/>" + result["debug"];
		    }
		    continue;
            }

            var rtype = (curUI() == "wp") ? "relatedres" : "srpresult";
	    out += resultHtml(result, rtype);
            // Get clean text (remove punct) to retrieve related entities (per result and overall for the query)
            // Backend will do text match b/w clean_text and entity names to compute related entities.
            var title_clean_txt = result["title"].replace(/[.,\/#!$%\^&\*;:{}=\-_`~()|\"\']/g, " ");
	    var descript_clean_txt = "";
            if (result.hasOwnProperty("description")) {  
		 descript_clean_txt = result["description"].replace(/[.,\/#!$%\^&\*;:{}=\-_`~()|\"\']/g, " ");
            }
            digest_arr.push(title_clean_txt + "|" +  descript_clean_txt);
            ++displayed_items;
            if (deb_level && deb_level > 1) {
		out += '<br/>' + result["debug"];
	    }
	    result_ytids.push(result["ytid"]);
      }
      digest["content"] = out;
      digest["result_ytids"] = result_ytids;
      digest["digest_arr"] = digest_arr;
      return;
}
 

function digestSearchResponse(response) {
        var out = "";
        var digest_arr = [];
        var result_ytids = [];
        var result_channels = {};
        digest = {};
        if (!response.items) {
          return;
        }
        var displayed_items = 0;
        for(var i = 0; i < response.items.length; i++) {
            var item = response.items[i];
            var result = parseYoutubeSearchResponse(item);
            
	    if (result.hasOwnProperty("failed")) {
		    if (deb_level && deb_level > 1) {
		      out += "<br/>" + result["debug"];
		    }
		    continue;
            }

            var rtype = (curUI() == "wp") ? "relatedres" : "srpresult";
	    out += resultHtml(result, rtype);
            // Get clean text (remove punct) to retrieve related entities (per result and overall for the query)
            // Backend will do text match b/w clean_text and entity names to compute related entities.
            var title_clean_txt = result["title"].replace(/[.,\/#!$%\^&\*;:{}=\-_`~()|\"\']/g, " ");
	    var descript_clean_txt = "";
            if (result.hasOwnProperty("description")) {  
		 descript_clean_txt = result["description"].replace(/[.,\/#!$%\^&\*;:{}=\-_`~()|\"\']/g, " ");
            }
            digest_arr.push(title_clean_txt + "|" +  descript_clean_txt);
            ++displayed_items;
            if (deb_level && deb_level > 1) {
		out += '<br/>' + result["debug"];
	    }
	    result_ytids.push(result["ytid"]);
            result_channels[result["channel"]] = true;
      }
      digest["content"] = out;
      digest["result_ytids"] = result_ytids;
      digest["digest_arr"] = digest_arr;
      digest["channels"] = result_channels;
      return;
}
 
function composeHomepage() {
      var user_query = composeUserQuery();
	      
      if (user_query != "") {
         logSearch();
         Search(user_query, 50);
         return;
      }
      var content_html = watchHistoryHtml(srp_results_per_row);
      $(".watch_history").html(content_html);
      for(var i = 0; i < context_entities.length; i++) {
            var name = entity_icon2name[context_entities[i]];
	    if (name == "") {
	       continue;
	    }
	    Search(name, srp_results_per_row);  
      }
}
        
function SearchLocalDB(query, num_results) {
	var php_file = ToggleDevPhp("entity_results");
        var request = {};
        request["q"] = query;
        $.ajax({
	  url: php_file,
	  dataType: 'json',
	  async: true,
	  data: request,
	  success: handleEntityResultsResponse
	});
}

function logSearch() {
      if (!isDebugIp(myip)) {
	      var logging_params = fillInLoggingParams();
	      logging_params["etype"] = 'qr';
	      var php_file = ToggleDevPhp("logging");
	      $.post(php_file, logging_params);
      }
}

function Search(query, num_results) {
      if (cn_ver > 0) {
        SearchLocalDB(query, num_results);
        return;
      }
      query = convertBadWord(query);
          
      var lang = $(".lang_selection").val();
      var ctxt = (context === undefined) ? "" : context;
      var url = YTSearchUrl(query, ctxt, lang, num_results);
      if (deb_level && deb_level > 0) {
        $("#content").prepend("<b>" + url + "</b><br/>");
      }
      $.get(url, handleSearchResponse);
}

function logImpressions(digest) {
      if (!isDebugIp(myip)) {
	      var logging_params = fillInLoggingParams();
	      logging_params["etype"] = 'im';
	      logging_params["impressions"] = digest["result_ytids"].join(",");
	      var php_file = ToggleDevPhp("logging");
	      $.post(php_file, logging_params);
      }
}

function logEntityClick(icon) {
      if (!isDebugIp(myip)) {
	      var logging_params = fillInLoggingParams();
	      logging_params["etype"] = 'ec';
	      logging_params["query"] = icon;
	      var php_file = ToggleDevPhp("logging");
	      $.post(php_file, logging_params);
      }
}

function fetchEggs(digest) {
        result_entities = [];
	// Request related entity
        var user_query = composeUserQuery();
	var http = new XMLHttpRequest();	
	var fetch_eggs_url = ToggleDevPhp("fetch_eggs");
        var channels = Object.keys(digest["channels"]).map(function(x){return x;}).join(',');
	var params = "&q=" + user_query + "&digests=" + digest["digest_arr"].join(",") + "&lang=" + $(".lang_selection").val();
	// var params = "&q=" + user_query + "&channels=" + channels + "&lang=" + $(".lang_selection").val();
	http.open("POST", fetch_eggs_url, true);

	//Send the proper header information along with the request
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	http.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
		if (this.responseText.length > 0) {
		   result_entities = this.responseText.split(",");
		}
	    }
	}
	http.send(params);
}

function handleEntityResultsResponse(response) {
	digestLocalDBSearchResponse(response);
        var cur_class = curSrpClass();
        $(curSrpClass()).append(digest["content"]);
        logImpressions(digest);
        fetchEggs(digest);
}

function handleSearchResponse(response, status) {
	digestSearchResponse(response);
        $(curSrpClass()).append(digest["content"]);
        logImpressions(digest);
        fetchEggs(digest);
}

function parseDocURI(uri) {
  var res = {};

  if (!uri) {
    return res;
  }

  if (uri.indexOf("#") == uri.length - 1) {
    uri = uri.slice(0, uri.length -1);
  }
  if (uri.indexOf("cse") >= 0 && uri.indexOf("html") >=0) {
     res["developer"] = true;
  }

  var homepage_urls = ["http://openit.today", "http://openit.today/", "https://openit.day", "https://openit.today"];
  if (homepage_urls.indexOf(uri) >= 0) {
     res["hp"] = true;
  }

  var wordidx_after_domain = 0;
  var before_params = 0;
  if (uri.indexOf("121.43.182.10") >= 0) {
    cn_ver = 1;
    $(".lang_selection").hide();
    $(".chinese_logo").show();
  }
  if (uri.indexOf("openit.today") >= 0) {
    wordidx_after_domain = uri.indexOf("openit.today") + 13;
    before_params = uri.indexOf("?");
  } else {
    // This server has no domain, uri contains a IP address
    var last_dot_idx = uri.lastIndexOf(".");
    if (uri.slice(last_dot_idx).indexOf("/") >= 0) {
      wordidx_after_domain = last_dot_idx + uri.slice(last_dot_idx).indexOf("/") + 1;
      before_params = uri.indexOf("?");
    }
  }
  if (wordidx_after_domain < uri.length) {
    var temp = "";
    if (before_params < 0) {
      temp = uri.slice(wordidx_after_domain);
    } else {
      temp = uri.slice(wordidx_after_domain, before_params);
    }
    if (temp.indexOf(".html") < 0) {
      res["query"] = temp;
    }
    if (adwords_terms.hasOwnProperty(res["query"])) {
      res["query"] = adwords_terms[res["query"]];
    }
  }
  return res;
}

$(document).ready(function() {
        // This should absolutely be the first line of code!!!
        getUrlParameter();

        setPlayerSize();
	var ret = resultsPerRow($(window).width() * 0.8);
	srp_results_per_row = ret[0];
	icons_per_row = ret[1];
        var getip = "1";
        var uri_parsed = parseDocURI(uri);
        if (uri_parsed.hasOwnProperty("developer")) {
          getip = "developer";
        }
        if (uri_parsed.hasOwnProperty("query")) {
          cgi_q = uri_parsed["query"];
        }
        if (cgi_q) {
          $("#q").val(cgi_q);
        }
        if (isDebugIp(myip)) {
	  getip = peerOffDebIpPrefix(myip);
        }
        var req_key = composeUserQuery();
        if (prestore_entities.hasOwnProperty(req_key)) {
          req_key = prestore_entities[req_key];
        }
        RequestEntitySuggestion(req_key /*query*/,
                                req_key == "" ? false : true /*async*/,
                                getip,
                                1 /*context*/,
                                1 /*general*/,
                                1 /*history*/);
        general_entities_style_backup = $("#general_entities").cssAll('position,top,left,bottom,border-style,border,margin,padding,overflow');
        // cgi_q is for debuging or scraping for analysis
        fillingBadWordsDict();
        contextSwitchTo(1);
        composeHomepage();
        if (!isDebugIp(myip)) {
		var logging_params = fillInLoggingParams();
	        // "h" means "home page visit"
	        logging_params["etype"] = 'h';
		var php_file = ToggleDevPhp("logging");
		$.post(php_file, logging_params);
        }
        
    // Switch tabs 
    $(".tablinks").on('click touchend', function(){
      var idx = $(this).index(".tablinks");
      if (curUIStatus() == idx) {
        return;
      }
      contextSwitchTo(idx);
    });

    // Click anywhere on the top bar to close watchpage
    $("body").on('click touchend', '.back_to_kg', function(){
        contextSwitchTo(0);
	return;
    });

    // Click anywhere on the top bar to close watchpage
    $("body").on('click touchend', '.back_to_hp', function(){
        contextSwitchTo(1);
	return;
    });

	$("body").on('keyup', '.querybox, .refinement_query', function(e) {
            var event = e || window.event;
            var enter_pressed = false;
            var start_search = false;
	    if (  event.type == "keyup" ) {
	        var charCode = event.which || event.keyCode;
	        if (charCode == 13 ) {
                    enter_pressed = true;	
            	    start_search = true;
                }
            }

            var cur_class = $(this).attr("class");
            var query_entity_idx = cur_class == "refinement_query" ? 1 : 0;

	    if (!enter_pressed) {
	      var query = $(this).val();
              if (!query) {
                return;
              }
              if (entity_name2icon.hasOwnProperty(query)) {
		 var image = 'url("entity_icons/' + entity_name2icon[query] + '")';
		 $(this).css("background-image", image);
		 $(this).css("background-position", 'right');
                 // start_search = true;
              } else {
		 $(this).css("background-image", "none");
                 RequestEntitySuggestion(query,
                                         true, /*async*/
					 peerOffDebIpPrefix(myip), /*getip*/
                                         "1", /*context*/
                                         "0", /*general*/
                                         "0"  /*history*/);
              }
            }
	    if ( start_search) {
	      $(this).blur();
              if (curUI() == "wp") {
		  $(".watchpage_topbar").html(composeWatchpageTopBar());
              } else {
                  contextSwitchTo(1);
              }
	      try {
		  $(curSrpClass()).html("");
		  $(".watch_history").hide();
		  logSearch();
		  Search(composeUserQuery(), 50);
	      }
	      catch (err) {
		  var out =  "<b>  Oops, nothing found here </b><br/><br/>";
         	  $(curSrpClass()).html(out);
	      }
            }
	});
        
	$("body").on('click touchend', '#videojs_player', function(){
		  // VideoJS("videojs_player", {"controls" : true, "autoplay" : true, "preload" : "auto", "width":player_width.toString(), "height":player_height.toString()}, function() {
		  VideoJS("videojs_player", {"controls" : true, "autoplay" : true, "preload" : "auto"}, function() {
		    $(this).load();
		  });
	});

	function composeIframeWindow(ytid, local_cache) {
	  if (local_cache != "true") { 
		  return '<iframe class="iframe-window"  src="' + embedYTLink(ytid) + '" ></iframe>';
	  }
	  // return '<video id="videojs_player" class="video-js vjs-default-skin" width="600" height="400"><source src="videos/' + ytid + '.mp4" type="video/mp4"> </video>';
	  return '<video id="videojs_player" class="video-js vjs-default-skin" width="' + player_width.toString() + '" height="' + player_height.toString() + '"><source src="videos/' + ytid + '.mp4" type="video/mp4"> </video>';
	}

	function handleVideoClick(element) {
	      var ytid = element.attr('ytid');
	      if (ytid === "") {
		return;
	      }
	      var local_cache = element.attr("local_cache");
	      var rtype = element.attr("rtype");
	      if (curUI() == "wp") {
		$(".iframe").html(composeIframeWindow(ytid, local_cache));
	      } else {
		      $("#refine_q").val("");
		      contextSwitchTo(3);
		      var watchpage_topbar = '<div class="watchpage_topbar">' + composeWatchpageTopBar() + '</div>';
		      var iframe = '<div class="iframe">' + composeIframeWindow(ytid, local_cache) + '</div>';
		      var refine_entity_bar = composeRefinementBar();
		      var related_bar = '<div class="relatedbar">' + digest["content"] + '</div>';
		      $("#watchpage").html(watchpage_topbar + refine_entity_bar + iframe + related_bar);
	      }
	   
	      if (!isDebugIp(myip)) {
		      var logging_params = fillInLoggingParams();
		      logging_params["etype"] = (rtype == "srpresult" ? 'sc' : 'rc');
		      logging_params["click_url"] = element.attr("ytid");  
		      logging_params["title"] = element.attr("title");
		      logging_params["displaylink"] = element.attr("displaylink");
		      var myidx = element.index(".resultwrap");
		      logging_params["click_pos"] = myidx;  
		      var php_file = ToggleDevPhp("logging");
		      $.post(php_file, logging_params);
	       }
	}

        // Handle result click, show watchpage window
        $("body").on('click', '.resultwrap', function(){
	     handleVideoClick($(this));
        });

        // Whenever user switch language,we should do a Request entity suggestion for two reasons
        // A: Some entities does not have translation in user pick language they should not be shown
        // B: Retrieve entity name in user language for search.
	$(".lang_selection").change(function() {
	  RequestEntitySuggestion("",
	  			  true, /*async*/
				  peerOffDebIpPrefix(myip), /*getip*/
				  "1", /*context*/
				  "1", /*general*/
				  "0"  /*history*/);
	});
});

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-87272606-1', 'auto');
  ga('send', 'pageview');

