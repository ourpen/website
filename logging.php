<?php

header("Content-Type: text/html; charset=UTF-8");

// Connecting, selecting database
$link = mysql_connect('localhost', 'root', 'PenCanFly')
    or die('Could not connect: ' . mysql_error());
mysql_select_db('wordpress-db') or die('Could not select database');
mysql_set_charset('utf8', $link);
mysql_query("SET NAMES 'utf8'", $link); 
mysql_query('SET CHARACTER SET utf8', $link);

$insert = "INSERT INTO open_logging ";

$ip = '';
$query = '';
$qtype = '';
$lang = '';
$keys = array();
$values = array();
$icon_name = "";

if ($_POST) {
  foreach ($_POST as $key => $value) {
    array_push($keys, $key);
    array_push($values, $value);
    if ($key == "query") {
      // $query = $value;
       $query = utf8_encode($value);
    } else if ($key == "ip") {
      $ip = $value;
    } else if ($key == "lang") {
      $lang = $value;
    } else if ($key == "qtype") {
      $qtype = $value;
    } else if ($key == "entity_name" && $value !== '') {
      $icon_name = $value;
    }
  }
}

$first_key = True;
$insert .= "(";
while (!empty($keys)) {
	if (!$first_key) {
	    $insert .= ",";
	} else {
	    $first_key = False;
	}
	$insert .= array_shift($keys);
}
$insert .= ") ";


$first_key = True;
$insert .= " VALUES (";
while (!empty($values)) {
	if (!$first_key) {
	    $insert .= ",";
	} else {
	    $first_key = False;
	}
	$insert .= ('"' . array_shift($values) . '"');
}
$insert .= ") ";


// logging query or click event
$result = mysql_query($insert, $link);
if (!$result) {
	error_log("logging messed up with:" . $insert); 
}

// logging query terms;
if ($ip !== '' && $query !== '' && $qtype == 'b') {
	$insert = "INSERT INTO user_vocab (id, word) VALUES ";
	$insert2 = "INSERT INTO user_profile (id, category) VALUES ";

	$tokens = explode(" OR ", trim($query));

	$first_key = True;
	foreach ($tokens as $orig_token) {
                $token = trim($orig_token);
                if ($token == "" || $token == " " || strlen($token) <= 1) {
                  continue;
                }
		if (!$first_key) {
		    $insert .= ",";
		}
		$category = "";
		$select = "SELECT root_cat  FROM  entity_icons where name_" . $lang . "  = '" . $token . "'";

		$result = mysql_query($select, $link);
                if (!$result) { 
			error_log("query messed up with:" . $select); 
		} else {
			$count=mysql_num_rows($result);
			if($count>0) {
			  if($row = mysql_fetch_assoc($result)) {
			     $category = $row["root_cat"];
			  }
			}
                }
                //("$ip", "$word")
                //("24.130.227.186", "friends"), 
		$insert .= ('("' . $ip. '", "' . $token . '")');
		$insert2 .= ('("' . $ip. '", "' . $category . '")');
	        $first_key = False;
	}
        $insert .= " ON DUPLICATE KEY UPDATE querycnt=querycnt+1";
        $insert2 .= " ON DUPLICATE KEY UPDATE querycnt=querycnt+1";
        if (!$first_key) {
		$result = mysql_query($insert, $link);
		if (!$result) {
			error_log("logging messed up with:" . $insert); 
		}
		$result = mysql_query($insert2, $link);
		if (!$result) {
			error_log("logging messed up with:" . $insert2); 
		}
        }
} else {
        error_log(" ip or query is empty");
}

// logging icon clicks;
if ($icon_name !== '' && $ip != "developer") {
	$update = "UPDATE entity_icons SET clkcnt = clkcnt + 1 WHERE name = '" . $icon_name . "'";

	$result = mysql_query($update, $link);
	if (!$result) {
		error_log("logging messed up with:" . $update); 
	}
}

mysql_close($link);

?>


