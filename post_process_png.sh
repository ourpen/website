#!/bin/bash
## Example usage:
## ./post_process_png.sh /var/www/html/entity_raw/images/*.png
FOLDER=$1
for f in $FOLDER/*.png
do
	echo "Processing $f"
        lower=`echo $f | tr '[:upper:]' '[:lower:]'`
        echo $lower
        new_name=`echo $lower | sed 's/\"\/\_\/,//g' | sed 's/\./ /g'`
        echo $new_name
         tokens=(  $new_name )
          unset tokens[${#tokens[@]}-1]
        len=${#tokens[@]}
        cnt=0
        fname="."
        echo $len
        while [ $cnt -lt ${len} ]; do
           if [ $cnt -gt 0 ]; then
             fname+="_"
           fi
           echo ${tokens[$cnt]}
           fname+=${tokens[$cnt]}
	   let cnt=cnt+1
           if [ $cnt -gt 1 ]; then
             break;
           fi
        done
        fname+=".png"
        echo $fname 
        mv "$f" ${fname}
done
