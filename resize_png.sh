#!/bin/bash
## Example usage:
## ./resize_png.sh /var/www/html/entity_raw/images/*.png
#FILES=$1
for f in *.png
do
	echo "Processing $f"
        mogrify -resize 160X160 "$f"
done

