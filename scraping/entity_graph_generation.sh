#!/bin/bash
IN=$1
OUT=$2
while read p; do
	echo "Processing $p"
        lower=`echo $p | tr '[:upper:]' '[:lower:]'`
         IFS=',' read -r -a array <<< "$lower"
        key=${array[0]}
        for element in "${array[@]}"
		do
                   if [ "$element" != "$key" ]
		   then 
		      echo "$key,$element" >> $OUT
                   fi
		done
done < $IN

