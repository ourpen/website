#!/bin/bash
## Example usage:
## ./post_process_png.sh /var/www/html/entity_raw/images/*.png
IN=$1
OUT=$2
prev=""
cnt=1
while read p; do
   echo "Processing $p"
   if [ "$p" != "$prev" ]
   then 
      echo "$prev,unknown,$cnt" >> $OUT
      cnt=1
      prev=$p
   else
     cnt=$((cnt+1))
   fi
done < $IN
